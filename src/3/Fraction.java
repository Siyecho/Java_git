import java.util.Scanner;

public class Fraction
{
	public static void main(String[] args)
	{
		System.out.print("Input numerator and denominator: ");
		Scanner scan = new Scanner(System.in);
		
		int numerator=scan.nextInt();
		int denominator=scan.nextInt();
		
		System.out.print("The fraction is "+numerator+"/"+denominator);
	}
}