import java.util.Scanner;

public class Square
{
	public static void main(String[] args)
	{
		System.out.print("Input a number in int: ");
		Scanner scan=new Scanner(System.in);
		
		int length=scan.nextInt();
		
		System.out.println("The circumference is "+4*length);
		System.out.println("The area is "+length*length);
	}
}