import java.util.Scanner;
import java.util.Random;

public class PP4_15
{
	public static void main(String[] args)
	{
		Scanner scan=new Scanner(System.in);
		
		Random rand=new Random();
		
		int a,b,c;
		System.out.print("Enter 'g' to start game(q to quit): ");
		
		String temp=scan.nextLine();
		char userChoice=temp.charAt(0);
		
		if(userChoice=='q')
			System.out.println("Have fun next time!");
		
		else
		{
			while(userChoice=='g')
			{
				a=rand.nextInt(9);
				b=rand.nextInt(9);
				c=rand.nextInt(9);
			
				System.out.println("a	b	c\n"+a+"	"+b+"	"+c+"	");
			
				if(a==b&&b==c)
					System.out.println("All Same!");
				else if(a==b||b==c||a==c)
					System.out.println("Double Win!");
				else
					System.out.println("Try Again!");
				
				System.out.print("Enter 'g' to start game(q to quit): ");
				temp=scan.nextLine();
				userChoice=temp.charAt(0);
				
				if(userChoice=='q')
				{
					System.out.println("Have fun next time!");
					break;
				}
			}
		}
	}
}