package com.common.util;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.widget.Toast;

import java.util.List;

/**
 * Created by user on 2018/4/13 0013.
 */

public class AddGPS extends Activity {
    public String getGPS(){

        //定位都要通过LocationManager这个类实现
        LocationManager locationManager;
        String provider;
        //获取定位服务
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        //获取当前可用的位置控制器
        List<String> list = locationManager.getProviders(true);

        if (list.contains(LocationManager.GPS_PROVIDER)) {
            //是否为GPS位置控制器
            provider = LocationManager.GPS_PROVIDER;
        }
        else if (list.contains(LocationManager.NETWORK_PROVIDER)) {
            //是否为网络位置控制器
            provider = LocationManager.NETWORK_PROVIDER;

        } else {
            //Toast.makeText(this, "请检查网络或GPS是否打开",
            //        Toast.LENGTH_LONG).show();
            return null;
        }
        Location location = locationManager.getLastKnownLocation(provider);
        if (location != null) {
            //获取当前位置，这里只用到了经纬度
            String string =location.getLongitude() + ", "
                    + location.getLatitude();

            return  string;
        }

        return null;
    }
}
