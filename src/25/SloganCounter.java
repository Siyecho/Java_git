//*********************************************************
//
//	SloganCounter.java		Java Foundations
//
//	Demonstrates the use of the static modifier.
//
//*********************************************************

public class SloganCounter
{
	//-----------------------------------------------------
	//
	//	Creates several Slogan objects and prints the number
	//	of objects that were created.
	//
	//-----------------------------------------------------
	
	public static void main(String[] args)
	{
		Slogan obj;
		
		
		obj = new Slogan ("Remember the Alomo.");
		System.out.println(obj);
		
		
		obj = new Slogan ("Don't Worry.");
		System.out.println(obj);
		
		
		obj = new Slogan ("Live Free or Die.");
		System.out.println(obj);
		
		
		obj = new Slogan ("Talk is Cheap.");
		System.out.println(obj);
		
		
		obj = new Slogan ("Write Once.");
		System.out.println(obj);
		
		
		System.out.println();
		System.out.println ("Slogan created: " + Slogan.getCount());
		
		
		for(int i=0;i<1;i++)
		{
			//System.out.println();
		}
	}
}