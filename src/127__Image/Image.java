public class Image {

    private static Bitmap imageBitmap = null;//拍照获取的bitmap
    private static Intent intent;

    public static int getMiniSize(String imagePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, options);
        return Math.min(options.outHeight, options.outWidth);
    }

    public static boolean isSquare(String imagePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, options);
        return options.outHeight == options.outWidth;
    }

    //图片是不是正方形
    public static boolean isSquare(Uri imageUri) {
        ContentResolver resolver = App.getApp().getContentResolver();

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeStream(resolver.openInputStream(imageUri), null, options);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return options.outHeight == options.outWidth;
    }

    //保存图片文件
    public static String saveToFileInNextStep(String fileFolderStr, boolean isDir, Bitmap croppedImage, String orgPicPath) throws Exception {
        File jpgFile;
        if (isDir) {
            File fileFolder = new File(fileFolderStr);

            //Date date = new Date();
            //SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss"); // 格式化时间

            //2018.4.27 15开头的文件
            //String filename = System.currentTimeMillis() + ".jpg";
            String filename = String.valueOf(System.currentTimeMillis());
            //CameraUtils.saveBitmap(imageBitmap, filename);


            if (!fileFolder.exists()) { // 如果目录不存在，则创建一个名为"finger"的目录
                FileUtils.getInst().mkdir(fileFolder);
            }
            jpgFile = new File(fileFolder, filename);
        } else {
            jpgFile = new File(fileFolderStr);
            if (!jpgFile.getParentFile().exists()) {
                FileUtils.getInst().mkdir(jpgFile.getParentFile());
            }
            if (!jpgFile.exists()){
                jpgFile.createNewFile();
            }
        }

        FileOutputStream outputStream = new FileOutputStream(jpgFile); // 文件输出流
        croppedImage.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

        //saveExif(jpgFile);

        IOUtil.closeStream(outputStream);
        addExif.writeExif(new File(jpgFile.getPath()));
        addExif.exifEnterfaceAddExif(jpgFile.getPath());

        //jpgFile.renameTo(new File(jpgFile.getPath()+"SJPG.jpg"));
        PhotoPrivacy.doPrivacy(orgPicPath, jpgFile.getPath());

        //File tempFile = new File(jpgFile.getPath() + "SJPG.jpg");
        //PhotoPrivacy.doPrivacy(tempFile.getPath());
//
        //File finalJpgFile = new File(jpgFile.getPath() + "SJPG.jpgSJPG.jpg");
        //tempFile.delete();
        //finalJpgFile.renameTo(new File(jpgFile.getPath() + "SJPG.jpg"));
        //jpgFile.delete();

        //return finalJpgFile.getPath();
        //return jpgFile.getPath()+"SJPG.jpg";

        //删除下一步时的无后缀名照片
        String filePath = jpgFile.getPath();
        jpgFile.delete();
        return filePath + "SJPG.jpg";

    }

    //保存图片文件
    public static String saveToFileWhileTakingPicture(String fileFolderStr, boolean isDir, Bitmap croppedImage) throws Exception {
        File jpgFile;
        if (isDir) {
            File fileFolder = new File(fileFolderStr);

            //Date date = new Date();
            //SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss"); // 格式化时间

            //2018.4.27 15开头的文件
            //String filename = System.currentTimeMillis() + ".jpg";
            String filename = String.valueOf(System.currentTimeMillis());
            //CameraUtils.saveBitmap(imageBitmap, filename);


            if (!fileFolder.exists()) { // 如果目录不存在，则创建一个名为"finger"的目录
                FileUtils.getInst().mkdir(fileFolder);
            }
            jpgFile = new File(fileFolder, filename);
        } else {
            jpgFile = new File(fileFolderStr);
            if (!jpgFile.getParentFile().exists()) { // 如果目录不存在，则创建一个名为"finger"的目录
                FileUtils.getInst().mkdir(jpgFile.getParentFile());
            }
        }

        FileOutputStream outputStream = new FileOutputStream(jpgFile); // 文件输出流
        croppedImage.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        IOUtil.closeStream(outputStream);
        addExif.writeExif(new File(jpgFile.getPath()));
        addExif.exifEnterfaceAddExif(jpgFile.getPath());

        PhotoPrivacy.doPrivacy(jpgFile.getPath(), jpgFile.getPath());
        File newJpgFile = new File(jpgFile.getPath() + "SJPG.jpg");
        jpgFile.delete();

        return newJpgFile.getPath();
    }
}