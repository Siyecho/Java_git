import java.util.Scanner;

public class Money
{
	public static void main(String[] args)
	{
		System.out.print("Input in order: ");
		
		int one_cent,two_cent,five_cent,one_dime,five_dime,one_Yuan;
		
		Scanner scan=new Scanner(System.in);
		one_cent=scan.nextInt();
		two_cent=scan.nextInt();
		five_cent=scan.nextInt();
		one_dime=scan.nextInt();
		five_dime=scan.nextInt();
		one_Yuan=scan.nextInt();
		
		int total=one_cent+2*two_cent+5*five_cent+10*one_dime+50*five_dime+100*one_Yuan;
		System.out.println("You input "+(total/100)+" Yuan "+((total%100)/10)
			+" dime "+((total%100)%10)+" cent.");
	}
}