//************************************************************
//  Pizza.java       Java Foundations
//
//  Represents a pizza, which is a food item.Used to demonstrates
//  indirect referencing through inheritance.
//************************************************************
public class Pizza extends FoodItem{
    //--------------------------------------------------------
    //  Sets up a pizza with the specified amount of fat (assumes
    //  eight servingd).
    //--------------------------------------------------------
    public Pizza (int fatGrams)
    {
        super(fatGrams, 8);
    }
}
