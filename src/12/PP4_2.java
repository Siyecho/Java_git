import java.util.Scanner;

public class PP4_2
{
	public static void main(String[] args)
	{
		Scanner scan=new Scanner(System.in);
		
		System.out.println("Enter a year(0 to quit): ");
		int year=scan.nextInt();
		
		while(year!=0)
		{
			if(year<1582)
			{
				System.out.println("Wrong year entered.Try again:");
				year=scan.nextInt();
				continue;
			}
			
			if(year%100==0)
			{
				if(year%400==0)
					System.out.println("BINGO!");
			}
			else if(year%4==0)
				System.out.println("BINGO!");
			else
				System.out.println("It is not wanted.Try again:");
			
			year=scan.nextInt();
		}
		
		System.out.println("Bye!");
	}
}

		