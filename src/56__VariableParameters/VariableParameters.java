public class VariableParameters {
    //--------------------------------------------------------
    //  Creates two Family objects using a constructor that
    //  accepts a variable number of String as parameters.
    //--------------------------------------------------------
    public static void main (String[] args)
    {
        Family lewis = new Family("John", "Sharon", "Justin", "Kayla",
                "Nathan", "Smantha");

        Family camden = new Family("Stephen", "Annie", "Matt", "Mary",
                "Simon", "Lucy", "Ruthie", "Sam", "David");

        System.out.println(lewis);
        System.out.println();
        System.out.println(camden);
    }
}
