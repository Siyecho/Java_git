

import java.io.*;
import java.util.Iterator;

import com.drew.imaging.jpeg.JpegMetadataReader;
import com.drew.imaging.jpeg.JpegProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import mediautil.image.jpeg.LLJTran;
import mediautil.image.jpeg.LLJTranException;
import mediautil.image.jpeg.*;
import mediautil.gen.Rational;
import sun.swing.PrintingStatus;

import static mediautil.image.jpeg.AbstractImageInfo.NA;

public class PhotoPrivacy
{

    /**
     * 主函数，测试调用的函数接口
     *
     * @param args
     */
    public static void main(String[] args) throws Exception{

        writePicExif("D:/test1.jpg", "D:/test2.jpg", "D:/test3.jpg");
        String a = readPicExif("D:/test3.jpg");
        System.out.println(a);
        readPic("D:/test002.jpg");
    }

    /**
     * ´¦Àí µ¥ÕÅ Í¼Æ¬
     *
     * @return void
     * @date 2015-7-25 ÏÂÎç7:30:47
     */

    private static void readPic(String PicFileName)
    {
        File jpegFile = new File(PicFileName);
        Metadata metadata;
        try
        {
            metadata = JpegMetadataReader.readMetadata(jpegFile);
            Iterator<Directory> it = metadata.getDirectories().iterator();
            while (it.hasNext())
            {
                Directory exif = it.next();
                Iterator<Tag> tags = exif.getTags().iterator();
                while (tags.hasNext())
                {
                    Tag tag = (Tag) tags.next();
                    System.out.println(tag);

                }

            }

        } catch (JpegProcessingException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    private static void writePicExif(String inPicFileName, String outPicFileName1, String outPicFileName2)throws Exception
    {

        //原文件
        InputStream fip = new BufferedInputStream(new FileInputStream(inPicFileName)); // No need to buffer
        LLJTran llj = new LLJTran(fip);
        try {
            llj.readSJPG(LLJTran.READ_INFO, true);
        } catch (LLJTranException e) {
            e.printStackTrace();
        }

        Exif exif = (Exif) llj.getImageInfo();

        // Set some values directly to gps IFD

        Entry e;
        // Set Latitude



        // Set Privacy
        e = new Entry(Exif.ASCII);
        e.setValue(0, "nspprivacy sjpeg");
        String a = e.toString();
        exif.setTagValue(Exif.PRIVACY,0, e, true);




        //设置具体的精度


        e = new Entry(Exif.RATIONAL);
        e.setValue(0, new Rational(31, 1));
        e.setValue(1, new Rational(21, 1));
        e.setValue(2, new Rational(3, 1));
        exif.setTagValue(Exif.GPSLatitude,-1, e, true);


        e = new Entry(Exif.ASCII);
        e.setValue(0, "abc");
        exif.setTagValue(Exif.GPSLatitudeRef,-1, e, true);

        // Set Longitude
        e = new Entry(Exif.ASCII);
        e.setValue(0, "afjkldajgagjklajg@jsalkfjkla#hfjakhfjaklsg");
        exif.setTagValue(Exif.GPSLongitudeRef,-1, e, true);

        //设置具体的纬度
        e = new Entry(Exif.RATIONAL);
        e.setValue(0, new Rational(120, 1));
        e.setValue(1, new Rational(58, 1));
        e.setValue(2, new Rational(11, 1));
        exif.setTagValue(Exif.GPSLongitude,-1, e, true);


        llj.refreshAppx(); // Recreate Marker Data for changes done

        //改写后的文件，文件必须存在
        InputStream in = new BufferedInputStream(new FileInputStream(outPicFileName1));
        OutputStream out = new BufferedOutputStream(new FileOutputStream(outPicFileName2));

        // Transfer remaining of image to output with new header.
        llj.xferInfo(in, out, LLJTran.REPLACE, LLJTran.REPLACE);

        fip.close();
        out.close();

        llj.freeMemory();
    }

    private static String readPicExif(String inPicFileName)throws Exception
    {

        //原文件
        InputStream fip = new BufferedInputStream(new FileInputStream(inPicFileName)); // No need to buffer
        LLJTran llj = new LLJTran(fip);
        try {
            llj.read(LLJTran.READ_INFO, true);
        } catch (LLJTranException e) {
            e.printStackTrace();
        }

        Exif exif = (Exif) llj.getImageInfo();
        //Entry e = exif.getTagValue(Exif.PrivacyPath, true);
        // Set some values directly to gps IFD
        Entry e = exif.getTagValue(Exif.PRIVACY, true);
        if (e != null)
            return e.toString();


        fip.close();


        llj.freeMemory();
        return NA;
    }
    
}