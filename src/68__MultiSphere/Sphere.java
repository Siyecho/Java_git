import java.lang.Math;

public class Sphere {
    private double diameter;
    private double vol;
    private double area;

    public Sphere (double dia)
    {
        diameter = dia;
    }

    public void setDiameter(double dia)
    {
        diameter = dia;
    }

    public double getDiameter()
    {
        return diameter;
    }

    public double volume()
    {
        vol =  4 / 3 * Math.PI * Math.pow(diameter, 3);
        return vol;
    }

    public double superficialArea()
    {
        area = 4 * Math.PI * Math.pow(diameter, 2);
        return area;
    }

    @Override
    public String toString() {
        String str = "The diameter is " + diameter + "\n"
                + "The volume is " + 4 / 3 * Math.PI * Math.pow(diameter, 3) + "\n"
                + "The superficial area is " + 4 * Math.PI * Math.pow(diameter, 2) + "\n";

        return str;
    }
}
