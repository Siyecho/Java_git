import java.util.Scanner;

public class TwoFloat
{
	public static void main(String[] args)
	{
		double first,second;
		Scanner scan=new Scanner(System.in);
		System.out.print("Input two float: ");
		first=scan.nextFloat();
		second=scan.nextFloat();
		System.out.println("The sum is "+(first+second)+"\n"
			+"The difference is "+(first-second)+"\n"
			+"The product is "+(first*second)+"\n");
	}
}