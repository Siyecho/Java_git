import java.util.Scanner;

public class PP3_4
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		double x1,y1,x2,y2;
		
		System.out.print("Input point 1 x and y: ");
		x1=scan.nextDouble();
		y1=scan.nextDouble();
		
		System.out.print("Input point 2 x and y: ");
		x2=scan.nextDouble();
		y2=scan.nextDouble();
		
		double x=(x2-x1)*(x2-x1);
		double y=(y2-y1)*(y2-y1);
		
		System.out.print("distance is "+Math.sqrt(x+y));
	}
}