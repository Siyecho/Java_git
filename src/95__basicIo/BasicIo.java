import java.io.*;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;


public class BasicIo {

    
	public static int s2n(byte[] buf, int offset, int length, boolean signed, boolean intel) {
		int val = 0;
		if (intel) {
			int shift = 0;
			for (int i=offset;i<(length+offset) && i<buf.length;i++) {
				val = val + ((buf[i] & 255) << shift);
				shift += 8;
			}
		} else {
			for (int i=0;i<length;i++)
				val = (val<<8) + (buf[offset+i] & 255);
		}
		if (signed) {
			int msb = 1 << (8*length - 1);
			if ((val & msb) > 0)
				val = val - (msb << 1);
		}
		return val;
	}

    
	public static void in2s(byte[] result, int offset, int value, int length) {
		for(int i=0; i<length; i++) {
			result[offset+i] = (byte)(value & 255);
			value >>= 8;
		}
	}

    
	public static void bn2s(byte[] result, int offset, int value, int length) {
		for(int i=0; i<length; i++) {
			result[offset+length-i-1] = (byte)(value & 255);
			value >>= 8;
		}
	}

    
	public static byte[] bn2s(int value, int length) {
		byte[] result = new byte[length];
		bn2s(result, 0, value, length);
		return result;
	}

    
	public static boolean isSignature(byte markerData[], int offset, String signature) {
		for (int i=0; i<signature.length(); i++) {
			if (signature.charAt(i) != (markerData[offset+i] & 255))
				return false;
		}
		return true;
	}

    
    public static long skip(InputStream is, long n) throws IOException {
        long lefttoskip = n;
        if (n > 0) {
            long skipLen;
            do {
                skipLen = is.skip(lefttoskip);
                if(skipLen < 1 && is.read() != -1)
                    skipLen = 1;
                lefttoskip -= skipLen;
            } while (lefttoskip > 0 && skipLen > 0);
        }
        return n - lefttoskip;
    }

    
    public static int read(InputStream is, byte b[], int off, int minBytes, int n)
        throws IOException {
        if(n < 0 || minBytes > n)
            throw new IOException("Invalid parameters minBytes = " + minBytes
                            + " n = " + n);
        int lefttoread = n;
        if (n > 0)  {
            int minLeft = minBytes;
            int readLen;
            do {
                readLen = is.read(b, off, lefttoread);
                if(readLen > 0)
                {
                    off += readLen;
                    lefttoread -= readLen;
                    minLeft -= readLen;
                }
            } while (minLeft > 0 && readLen >= 0);
        }
        return n - lefttoread;
    }

    
    public static int read(InputStream is, byte [] data) throws IOException {
        return read(is, data, 0, data.length, data.length);
    }

	public static final String FACTOR_ABVS[] = {"", "KB", "MB", "GB", "TB", "BB"};

	public static String convertLength(long l) {
		for (int i=0; ;i++) {
			if (l < 1024)
				return ""+l+FACTOR_ABVS[i];
			l /= 1024;
		}
	}

	public static int asInt(String str) {
		try {
			return (
				(ByteBuffer) ByteBuffer
					.allocate(4)
					.put(str.getBytes("ISO8859_1"))
					.order(ByteOrder.LITTLE_ENDIAN)
					.flip())
				.getInt();
		} catch (UnsupportedEncodingException uee) {
		}
		throw new IllegalArgumentException("Can't represent  " + str + " as int");
	}

	public static String asString(int i) {
		return Charset
			.forName("ISO8859_1")
			.decode((ByteBuffer) ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(i).flip())
			.toString();
	}
}
