import java.util.Scanner;

public class TempConverter
{
	public static void main(String[] args)
	{
		final int BASE=32;
		final double CONVERSION_FACTOR=9.0/5.0;
		
		System.out.print("Input fahrenheitTemp: ");
		Scanner scan=new Scanner(System.in);
		
		double fahrenheitTemp;
		double celsiusTemp;
		fahrenheitTemp=scan.nextDouble();
		
		celsiusTemp=(fahrenheitTemp-BASE)/CONVERSION_FACTOR;
		
		System.out.println("Fahrenheit Equivalent: "+fahrenheitTemp);
		System.out.println("Celsius Temperature: "+celsiusTemp);
		
	}
}