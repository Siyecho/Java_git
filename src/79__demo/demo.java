import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class demo {

    //读取xml文档中:<书名>JavaScript网页开发<书名> 节点中的值
    public void read1() throws Exception{

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse("src/book.xml");

        NodeList list = document.getElementsByTagName("书名");
        Node node = list.item(1);
        String content = node.getTextContent();
        System.out.println(content);
    }

    public void read2() throws Exception{

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse("src/book.xml");

        //得到根节点
        Node root = document.getElementsByTagName("书架").item(0);

        list(root);
    }

    private void list(Node node){
        if (node instanceof Element)
             System.out.println(node.getNodeName());

        NodeList list = node.getChildNodes();
        for (int i=0; i<list.getLength();i++){
            Node child = list.item(i);
            list(child);
        }
    }

    public void read3() throws Exception{

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse("src/book.xml");

        Element bookname = (Element) document.getElementsByTagName("书名").item(0);
        String value = bookname.getAttribute("name");
        System.out.println(value);
    }
}
