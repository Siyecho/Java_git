import java.util.Scanner;
import java.text.DecimalFormat;

public class PP3_5
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter the radius: ");
		double radius=scan.nextDouble();
		
		double cubage=Math.PI*Math.pow(radius,3)*4/3;
		double surface=Math.PI*Math.pow(radius,2)*4;
		
		DecimalFormat fmt = new DecimalFormat("0.####");
		
		System.out.println("The cubage is "+fmt.format(cubage));
		System.out.println("The surface is "+fmt.format(surface));
	}
}