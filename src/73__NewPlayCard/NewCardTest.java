import java.util.Scanner;

public class NewCardTest {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);

        System.out.println("Input how many cards you want: ");
        int cardNumber = scan.nextInt();

        DeckOfCard start = new DeckOfCard();

        start.shuffle();
        while (true)
        {
            start.deal(cardNumber);
            start.report();

            System.out.println("Input how many cards you want: ");
            cardNumber = scan.nextInt();
            if (cardNumber == 0)
                break;
        }
    }
}
