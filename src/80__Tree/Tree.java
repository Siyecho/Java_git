package javafoundations;

import java.util.Iterator;
import javafoundations.*;
import javafoundations.exceptions.*;
import jdk.nashorn.api.tree.BinaryTree;

public class Tree<T> implements BinaryTree<T> {
    protected BTnode<T> root;

    public LinkedBinaryTree()
    {
        root = null;
    }

    public LinkedBinaryTree (T element)
    {
        root = new BTNode<T>(element);
    }

    public LinkedBinaryTree (T element, LinkedBinaryTree<T> left,
                             LinkedBinaryTree<T> right)
    {
        root = new BTNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
    }

    public T getRootElement()
    {
        if (root == null)
            throw new EmptyCollectionException ("Get root operation "
             + "failed.The tree is empty.");

        return root.getElement();
    }

    public LinkedBinaryTree<T> getLeft()
    {
        if (root == null)
            throw new EmptyCollectionException ("Get left operation "
            + "failed.The tree is empty.");

        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root = root.getLeft();

        return result;
    }

    public T find (T target)
    {
        BTNode<T> node = null;

        if (root != null)
            node = root.find(target);

        if (node == null)
            throw new ElementNotFoundException ("Find operation failed. "
            + "No such element in tree.");

        return node.getELement();
    }

    public int size()
    {
        int result = 0;

        if (root != null)
            result = root.count();

        return result;
    }

    public Iterator<T> inorder()
    {
        ArrayIterator<T> iter = new ArrayIterator<T>();

        if (root != null)
            root.inorder(iter);

        return iter;
    }

    public Iterator<T>  levelorder()
    {
        LinkedQueue<BTNode<T>> queue = new LinkedQueue<BTNode<T>>();
        ArrayIterator<T> iter = new ArrayIterator<T>();

        if (root != null)
        {
            queue.enqueue(root);
            while (!queue.isEmpty())
            {
                BTNode<T> current = queue.dequeue();

                iter.add (current.getElement());

                if (current.getLeft() != null)
                    queue.enqueue(current.getLeft());
                if (current.getRight() != null)
                    queue.enqueue(current.getRight());
            }
        }

        return iter;
    }

    public Iterator<T> iterator()
    {
        return inorder();
    }
}
