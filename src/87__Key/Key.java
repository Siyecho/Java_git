class Key
{
    private int[] parrey = new int[18];// 32bit for 18 arrey
    private int[][] sbox = new int[4][256];// 32bit for 256 arrey in 4 boxes
    private int[] middle = new int[1042];
    private String keyphr;// 
    private String filename;
    private String ctext = "OrpheanBeholderScryDoubt";//
    //ctext = "OrpheanBeholderScryDoubt";//
    private int[] saltadd = new int[4];
    private int salttext[] = new int[6];
    
    //private static final String HMAC_SHA1 = "HmacSHA1";
    
    Key(String photo)
    {
        @SuppressWarnings("resource")
        Scanner phrase = new Scanner(System.in);
        System.out.println("enter the screte phrase");
        keyphr = phrase.next();// 
        //keyphr = "aaa";
        filename = photo;// 
        int i = 0;
        int j = 0;
        char[] fn = filename.toCharArray();
        
        for (; (i < saltadd.length) && (j < fn.length); i++)
        {
            saltadd[i] = 0;
            saltadd[i] += (int)(fn[(j)%fn.length]<<24);
            saltadd[i] += (int)(fn[(j+1)%fn.length]<<16);
            saltadd[i] += (int)(fn[(j+2)%fn.length]<<8);
            saltadd[i] += (int)fn[(j+3)%fn.length];
            j += 4;
        }
    }
    
    //
    int getitem(int pi[], int n)
    {
        int item = 0;
        int k;
        for (k = n - 1; k >= 0; k--)
        {
            item += pi[k] * 10000;
            pi[k] = item % (k * 2 + 1);
            item /= (k * 2 + 1);
            if (k > 0) item *= k;
        }
        return item;
    }
    
    //
    public void InitState()
    {
        int[] pi = new int[14589];
        int n = 14588;
        int i,remainder,item,j=0;
        for (i = 0; i < n; i++) pi[i] = 10000 / 5; // BASE / 5 == 2.000
        for (remainder = 0; n > 0; n -= 14){
            item = getitem(pi, n);
            middle[j] = remainder + item /10000;
            j++;
            //System.out.println(middle[j++]);
            remainder = item % 10000;
        }
        for(i = 0; i < 18 ;i++){
            parrey[i] = middle[i];		// parrey for first 18 numbers of pi
            //System.out.print(parrey[i]);
        }
        //System.out.println();
        //System.out.println("=============== sbox =================");
        for(j = 0; j < 4; j++){
            for(i = 0; i < 256; i++){
                sbox[j][i] = middle[18+j*256+i];   //sbox for next 1024 numbers of pi
                //System.out.print(sbox[j][i]);
            }
        }
        System.out.println();
    }
    
    //
    public void ExpandKey(int state, int salt[],String key)
    {
        int len, i,j;
        //private String key = new String;//string
        len = key.length();//
        int keydata = 0;
        j = 0;
        char[] keyc = key.toCharArray();
        for(i = 0; i < 18; i++)
        {
            keydata += keyc[j%len]<<24;//Integer.parseInt
            keydata += keyc[(j+1)%len]<<16;
            keydata += keyc[(j+2)%len]<<8;
            keydata += keyc[(j+3)%len];
            parrey[i] = keydata ^ parrey[i];//
            j = (j+4)%len;
            keydata = 0;
        }
        long res = 0;
        for(i = 0; i < 9; i++)
        {
            res = BlowFish(salt[(0+2*i)%4],salt[(1+2*i)%4]);//change salt or not?
            salt[(1+2*i)%4] = (int)(res & 0x0000000011111111);
            salt[(0+2*i)%4] = (int)(res >>32);
            parrey[i] = salt[(0+2*i)%4];
            parrey[i+1] = salt[(1+2*i)%4];
            salt[(2+2*i)%4] = salt[(0+2*i)%4] ^ salt[(2+2*i)%4];
            salt[(3+2*i)%4] = salt[(1+2*i)%4] ^ salt[(3+2*i)%4];
        }
        for(j = 0;j < 4; j++)
        {
            for(i = 0; i < 128; i++)
            {
                res = BlowFish(salt[(0+2*i)%4],salt[(1+2*i)%4]);//change salt or not?
                salt[(1+2*i)%4] = (int)(res & 0x0000000011111111);
                salt[(0+2*i)%4] = (int)(res >>32);
                sbox[j][i] = salt[(0+2*i)%4];
                sbox[j][i+1] = salt[(1+2*i)%4];
                salt[(2+2*i)%4] = salt[(0+2*i)%4] ^ salt[(2+2*i)%4];
                salt[(3+2*i)%4] = salt[(1+2*i)%4] ^ salt[(3+2*i)%4];
            }
        }
    }
    
    //
    public long BlowFish(int left, int right)
    {//*int place1, *int place2
        int i = 0;
        int right0 = right;
        int left0 = left;
        for(;i < 16; i++){
            right = left0 ^ parrey[i];
            left = right0 ^ F(right);
            right0 = right;
            left0 = left;
        }
        right = left0 ^ parrey[i++];
        left = right0 ^ parrey[i];
        long result = 0;
        result = (left << 32) + right;
        return result;
        //place1 = right;
        //palce2 = left;
    }
    
    //
    public int F(int num)
    {
        int a, b, c, d;
        a = num & 0x00000011;
        b = (num & 0x00001100)>>8;
        c = (num & 0x00110000)>>16;
        d = (num & 0x11000000)>>24;
        return ((sbox[0][a]+sbox[1][b]^sbox[2][c])+sbox[3][d]);
    }
    
    //
    public void getkey(int state, int cost)
    {//int salt[],  // 0, 1
        InitState();
        ExpandKey(state, saltadd,keyphr);//int state, int salt[]
        long times = 2 << (cost -1);
        for(long i = 0;i < times; i++)
        {
            int[] zerosalt = new int[4];
            //BlowFish(0,0);//left and right
            ExpandKey(state, zerosalt,filename);//128 bit filename
            ExpandKey(state, zerosalt,keyphr);
        }
        int j = 0;
        int k = 0;
        int q = 0;
        //int salttext[] = new int[6];
        char[] ctextc = ctext.toCharArray();
        for (k = 0; k < 6; k++)
        {
            salttext[k] = 0;
            salttext[k] += ctextc[q]<<24;
            salttext[k] += ctextc[q+1]<<16;
            salttext[k] += ctextc[q+2]<<8;
            salttext[k] += ctextc[q+3];
            q += 4;
        }
        //System.out.println("pre keydata "+salttext[0]+salttext[1]+salttext[2]+salttext[3]+salttext[4]+salttext[5]);
        long res = 0;
        for (; j < 64; j++)
        {
            for(k = 0; k < 6; k += 2)
            {
                //System.out.println("pre text "+salttext[k]+salttext[k+1]);
                res = BlowFish(salttext[k],salttext[k+1]);
                salttext[k] = (int)(res & 0x0000000011111111);
                salttext[k+1] = (int)(res >>32);
            }
        }
        //System.out.println("post keydata "+salttext[0]+salttext[1]+salttext[2]+salttext[3]+salttext[4]+salttext[5]);
    }
    
    //
    public byte[] getthreekeys(String plane) throws InvalidKeyException, NoSuchAlgorithmException
    { //int[] salttext
        int i;
        byte[] data,pdata;
        String datas = "";
        for (i = 0; i< salttext.length; i++)
        {
            datas += salttext[i];
        }
        data = datas.getBytes();//
        pdata = plane.getBytes();
        String HMAC_SHA1 = "HmacSHA1";
        SecretKeySpec signingKey = new SecretKeySpec(data, HMAC_SHA1);//
        Mac mac = Mac.getInstance(HMAC_SHA1);//
        mac.init(signingKey);//
        byte[] rawHmac = mac.doFinal(pdata);//
        //System.out.println("palne " + plane + ": key "+rawHmac+ " len:"+rawHmac.length);
        //change
        MessageDigest mdInst = MessageDigest.getInstance("MD5");//
        mdInst.update(rawHmac);//
        byte[] md = mdInst.digest();//
        //System.out.println("the lenth of the key" + plane + md + " is " + md.length);
        return md;
        //return rawHmac;
        //return MD5.encode(rawHmac);
    }
}