
import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;

import java.awt.image.*;
import java.awt.Rectangle;

import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.imageio.ImageIO;
//import javax.imageio.*;

import java.security.*;

import javax.crypto.Mac;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.BadPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.KeyGenerator;

class Thumbnail4
{
    private File file;
    private BufferedImage image;
    private int width, height;
    private int blocksize,subblocksize;
    private SecretKey secretKey;
    private int wnum, hnum, wwnum, hhnum, wmore, hmore,snum;
    String filename1;
    private int type;
    private String ftype;
    private int cc;
    
    
    Thumbnails(String filename, int bsize, int subsize) throws IOException
    {
        filename1 = filename;
        file  = new File(filename1);
        image = ImageIO.read(file);
        width = image.getWidth();
        height = image.getHeight();
        blocksize = bsize;
        subblocksize = subsize;
        ftype = filename1.substring(filename1.lastIndexOf(".")+1);//
        cc= 0;
        //System.out.println("get the file type "+ ftype);
        //bsize1 = bsize;
        
        if(isImage())
        {
            //int temp;
            wnum = width / blocksize;// not must properly
            wmore = width % blocksize;//宽的余数
            wwnum = ( wmore == 0)? wnum : wnum + 1;//在宽的这边，块的个数
            hnum = height / blocksize;
            hmore = height % blocksize;//长的余数
            hhnum = ( hmore == 0) ? hnum : hnum + 1;//在长的这边，块的个数
            if(subblocksize != 0)
            {
                snum = blocksize/subblocksize;
            }else
            {
                snum = 0;
            }
            
            //System.out.println("is image " + isImage() + " width:" + width+" height:"+height);
            // System.out.println( " wwnum " + wwnum +" hhnum "+hhnum +" wmore " + wmore + " hmore "+ hmore);
        }
        else
        {
            System.out.println("file is not a photo");
            //do something?
        }
        
    }
    
    //
    public boolean isImage()
    {
        if(image == null || width <= 0|| height <= 0)
        {
            System.out.println("not a image");
            return false;
        }
        return true;
    }
    
    //判断格式
    public boolean isRGB()// throws JpegProcessingException, IOException
    {
        //getType
        //int type;
        type = image.getType();
        //System.out.println("get the type of number " + type);
        //System.out.println("rgb type number is " + BufferedImage.TYPE_3BYTE_BGR);
        //if(type == BufferedImage.TYPE_3BYTE_BGR)
        if(type == BufferedImage.TYPE_INT_ARGB || type == BufferedImage.TYPE_INT_RGB)
        {
            System.out.println("true in isrgb");
            return true;
        }
        
        return false;
    }
    
    
}