
import java.util.Scanner;

public class PP4_5
{
	public static void main(String[] args)
	{
		Scanner scan=new Scanner(System.in);
		
		System.out.print("Enter a number: ");
		
		int num=scan.nextInt();
		
		if(num==0)
			System.out.println("The number you entered is 0.");
		
		else
		{
			if(num%2==0)
				System.out.println("The number you entered is even.");
			else
				System.out.println("The number you entered is odd.");
		}
	}
}