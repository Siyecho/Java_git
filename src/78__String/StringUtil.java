import java.util.ArrayList;  
import java.util.LinkedHashSet;  
import java.util.Set;  
import java.util.regex.Matcher;  
import java.util.regex.Pattern;  
  

public class StringUtil {  
    /** 
     * 功能描述：分割字符串 
     */   
    public static String[] split(String str, String splitsign) {  
        int index;  
        if (str == null || splitsign == null) {  
            return null;  
        }  
        ArrayList al = new ArrayList();  
        while ((index = str.indexOf(splitsign)) != -1) {  
            al.add(str.substring(0, index));  
            str = str.substring(index + splitsign.length());  
        }  
        al.add(str);  
        return (String[]) al.toArray(new String[0]);  
    }  
  
    /** 
     * 功能描述：替换字符串 
     */  
    public static String replace(String from, String to, String source) {  
        if (source == null || from == null || to == null)  
            return null;  
        StringBuffer str = new StringBuffer("");  
        int index = -1;  
        while ((index = source.indexOf(from)) != -1) {  
            str.append(source.substring(0, index) + to);  
            source = source.substring(index + from.length());  
            index = source.indexOf(from);  
        }  
        str.append(source);  
        return str.toString();  
    }  
  
    /** 
     * 替换字符串，能能够在HTML页面上直接显示(替换双引号和小于号) 
     */  
    public static String htmlencode(String str) {  
        if (str == null) {  
            return null;  
        }  
        return replace("\"", "&quot;", replace("<", "&lt;", str));  
    }  
  
    /** 
     * 替换字符串，将被编码的转换成原始码（替换成双引号和小于号） 
     */  
    public static String htmldecode(String str) {  
        if (str == null) {  
            return null;  
        }  
  
        return replace("&quot;", "\"", replace("&lt;", "<", str));  
    }  
  
    private static final String _BR = "<br/>";  
  
    /** 
     * 功能描述：在页面上直接显示文本内容，替换小于号，空格，回车，TAB 
     */  
    public static String htmlshow(String str) {  
        if (str == null) {  
            return null;  
        }  
  
        str = replace("<", "&lt;", str);  
        str = replace(" ", "&nbsp;", str);  
        str = replace("\r\n", _BR, str);  
        str = replace("\n", _BR, str);  
        str = replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;", str);  
        return str;  
    }  
  
    /** 
     * 功能描述：返回指定字节长度的字符串 
     */  
    public static String toLength(String str, int length) {  
        if (str == null) {  
            return null;  
        }  
        if (length <= 0) {  
            return "";  
        }  
        try {  
            if (str.getBytes("GBK").length <= length) {  
                return str;  
            }  
        } catch (Exception e) {  
        }  
        StringBuffer buff = new StringBuffer();  
  
        int index = 0;  
        char c;  
        length -= 3;  
        while (length > 0) {  
            c = str.charAt(index);  
            if (c < 128) {  
                length--;  
            } else {  
                length--;  
                length--;  
            }  
            buff.append(c);  
            index++;  
        }  
        buff.append("...");  
        return buff.toString();  
    }  
  
    /** 
     * 功能描述：判断是否为整数 
     */  
    public static boolean isInteger(String str) {  
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]+$");  
        return pattern.matcher(str).matches();  
    }  
  
    /** 
     * 判断是否为浮点数，包括double和float 
     */  
    public static boolean isDouble(String str) {  
        Pattern pattern = Pattern.compile("^[-\\+]?\\d+\\.\\d+$");  
        return pattern.matcher(str).matches();  
    }  
}  