import java.util.Random;

public class DeckOfCards {
    private final int LIMIT = 52;
    private int indexOriginal = 0;
    private String[] color = {"Spade", "Heart", "Diamond", "Club"};
    private int randomNum = 0;
    private int indexNew = 0;
    String msg;
    Random rand = new Random();

    Card[] poker = new Card[LIMIT];
    Card[] afterShuffle = new Card[LIMIT];

    public DeckOfCards()
    {

    }

    public void shuffle(){
        for (int colorIndex =0; colorIndex < 4; colorIndex++){
            for (int number = 1; number < 14; number++){
                poker[indexOriginal].setColor(color[colorIndex]);
                poker[indexOriginal].setNum(number);
                indexOriginal++;
            }
        }
        for (; indexNew < LIMIT; indexNew++){
            randomNum = rand.nextInt(LIMIT);
            while (poker[randomNum].getPoint() == 0)
                randomNum = rand.nextInt(LIMIT);

            afterShuffle[indexNew] = poker[randomNum];
            poker[randomNum].setNum(0);
        }
    }

    public void deal(int i){
        if (i <= LIMIT){
            for (; i < LIMIT; i++){
                afterShuffle[i] = new Card();
                msg = afterShuffle[i].toString();

                System.out.print(msg);
            }
        }
        else
            System.out.println("The number you input is overflow.");
    }

    public String report(){
        int rest = LIMIT - indexNew;
        return "There remains " + rest + "cards.\n";
    }
}
