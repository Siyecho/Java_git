//************************************************************
//  FoodItem.java       Java Foundations
//
//  Represents an item of food.Used as the parent of a derived class
//  to demonstrates indirect referencing.
//************************************************************
public class FoodItem {
    final private int CALORIES_PER_GRAM = 9;
    private int fatGrame;
    protected int servings;

    public FoodItem(int numFatGrams, int numServings)
    {
        fatGrame = numFatGrams;
        servings = numServings;
    }

    private int calories()
    {
        return fatGrame * CALORIES_PER_GRAM;
    }

    public int caloriesPerServing()
    {
        return (calories() / servings);
    }
}
