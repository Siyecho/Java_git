public class Card {
    private String color;
    private int point;

    public Card(){
        color="";
        point=0;
    }

    public void setColor(String tempColor)
    {
        color = tempColor;
    }

    public void setPoint(int tempPoint)
    {
        point = tempPoint;
    }

    public String getColor()
    {
        return color;
    }

    public int getPoint()
    {
        return point;
    }
}
