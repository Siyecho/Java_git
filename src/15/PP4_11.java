public class PP4_11
{
	public static void main(String[] args)
	{
		final int MAX=126;
		final int MIN=32;
		int row=0;
		for(int count=MIN;count<=MAX;count++)
		{
			System.out.print((char)count+"	");
			
			if(row<4)
				row++;
			else
			{
				System.out.println();
				row=0;
			}
		}
	}
}