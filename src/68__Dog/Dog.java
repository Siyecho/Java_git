public class Dog {
    private String dogName;
    private int age;

    public Dog(String name, int age)
    {
        dogName = name;
        this.age = age;
    }

    public void setDogName(String name)
    {
        dogName = name;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public String getDogName()
    {
        return dogName;
    }

    public int getAge()
    {
        return age;
    }

    public double manAgeToDogAge(int manAge)
    {
        return manAge / 7;
    }

    @Override
    public String toString() {
        String msg = "The dog's name is " + dogName + ".Age is " +
                this.age + ".\n";
        return msg;
    }
}
