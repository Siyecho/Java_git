import java.util.Scanner;

public class PP4_3
{
	public static void main(String[] args)
	{
		System.out.print("Enter an integer(equals to or more than 2): ");
		Scanner scan=new Scanner(System.in);
		
		int result=0;
		int number=scan.nextInt();
		if(number<2)
			System.out.println("The integer is too small.");
		else
		{
			for(int original=2;original<=number;original++)
			{
				if(original%2==0)
					result+=original;
			}
				
			System.out.println("The result is "+result);
		}
	}
}