import java.util.Scanner;

public class Times
{
	public static void main(String[] args)
	{
		System.out.print("Input hour(s),minute(s) and second(s): ");
		Scanner scan=new Scanner(System.in);
		
		int hour,minute,second;
		hour=scan.nextInt();
		minute=scan.nextInt();
		second=scan.nextInt();
		
		System.out.println(hour+" hour(s)"+minute+" minute(s)"+second+" second(s)"
			+"equals to "+(hour*60*60+minute*60+second)+" second(s)");
	}
}