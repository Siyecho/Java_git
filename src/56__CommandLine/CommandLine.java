public class CommandLine {
    //--------------------------------------------------------
    //  Prints all of command line arguments provided by the
    //  user.
    //--------------------------------------------------------

    public static void main (String[] args)
    {
        for (String arg : args)
            System.out.println(arg);
    }
}
