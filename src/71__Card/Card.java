import java.util.Random;

public class Card {
    Random rand = new Random();

    private String[] color = {"Spade", "Heart", "Diamond", "Club"};
    private int point;
    private int clnum;
    private String col;
    private int num;

    public Card()
    {
        col = getColor();
        num = getPoint();
    }

    private String getColor() {
        clnum = rand.nextInt(4);
        return color[clnum];
    }

    private int getPoint() {
        return rand.nextInt(12) + 1;
    }

    @Override
    public String toString() {
        return col + "\t\t" + num + "\n";
    }
}
