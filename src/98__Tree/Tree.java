import java.util.Stack;

public class Tree {

	public static void main(String[] args) {
		
		TreeNode p = init();
		pre1(p);
		System.out.println();
		mid1(p);
		System.out.println();
		post1(p);
		System.out.println();
		pre2(p);
		System.out.println();
		post2(p);
		
	}
	
	public static void visit(TreeNode root) {
		System.out.print(root.getData());
	}
	
	public static TreeNode init() {
		TreeNode a = new TreeNode(7);
		TreeNode b = new TreeNode(8);
		TreeNode c = new TreeNode(4,null,a);
		TreeNode d = new TreeNode(5);
		TreeNode e = new TreeNode(6,b,null);
		TreeNode f = new TreeNode(2,c,null);
		TreeNode g = new TreeNode(3,d,e);
		TreeNode h = new TreeNode(1,f,g);
		return h;
	}
	
	public static void pre1(TreeNode root) {
		if(root != null) {
			System.out.print(root.val);;
			pre1(root.getLchild());
			pre1(root.getRchild());
		}
	}
	
	public static void mid1(TreeNode root) {
		if(root != null) {
			mid1(root.getLchild());
			System.out.print(root.val);
			mid1(root.getRchild());
		}
	}
	
	public static void post1(TreeNode root) {
		if(root != null) {
			post1(root.getLchild());
			post1(root.getRchild());
			System.out.print(root.val);
		}
	}
	
	public static void pre2(TreeNode root) {
		Stack<TreeNode> stack = new Stack<TreeNode>();
		TreeNode p = root;
		while(p != null || stack.size()>0) {
			while(p!=null) {
				//visit(p);
				stack.push(p);
				p = p.getLchild();
			}
			if(stack.size()>0) {
				p = stack.pop();
				visit(p);
				p = p.getRchild();
			}
		}
	}
	
	public static void post2(TreeNode root) {
		Stack<TreeNode> stack1 = new Stack<TreeNode>();
		Stack<TreeNode> stack2 = new Stack<TreeNode>();
		TreeNode p = root;
		while(p != null || stack1.size()>0) {
			while(p!=null) {
				stack1.push(p);
				stack2.push(p);
				p = p.getRchild();
			}
			if(stack1.size()>0) {
				p = stack1.pop();
				p = p.getLchild();
			}
		}
		while(stack2.size()>0) {
			visit(stack2.pop());
		}
	}

}

class TreeNode{
	int val;
	TreeNode Lchild;
	TreeNode Rchild;
	TreeNode(int val){
		super();
		this.val = val;
		Lchild = null;
		Rchild = null;
	}
	TreeNode(int val,TreeNode lchild,TreeNode rchild){
		super();
		this.val = val;
		Lchild = lchild;
		Rchild = rchild;
	}
	
	public int getData() {
		return val;
	}
	
	public TreeNode getLchild() {
		return Lchild;
	}
	
	public void setLchild(TreeNode lchild) {
		this.Lchild = lchild;
	}
	
	public TreeNode getRchild() {
		return Rchild;
	}
	
	public void setRchild(TreeNode rchild) {
		this.Rchild = rchild;
	}
}
