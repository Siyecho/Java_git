package javafoundations;

import javax.swing.text.html.HTMLDocument;
import java.util.Iterator;

public class BinaryTree<T> extends HTMLDocument.Iterator<T> {
    public T getRootElement();

    public BinaryTree<T> getLeft();

    public BinaryTree<T> getRight();

    public boolean contains (T target);

    public T find (T target);

    public boolean isEmpty();

    public int size();

    @Override
    public String toString();

    public Iterator<T> preorder();

    public Iterator<T> inorder();

    public Iterator<T> postorder();

    public Iterator<T> levelorder();
}
