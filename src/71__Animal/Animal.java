public class Animal {
    private String animalName;
    private int age;

    public Animal(String name, int age)
    {
        animalName = name;
        this.age = age;
    }

    public void setAnimalName(String name)
    {
        animalName = name;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public String getAnimalName()
    {
        return animalName;
    }

    public int getAge()
    {
        return age;
    }

    public double manAgeToAnimalAge(int manAge)
    {
        return manAge / 7;
    }

    @Override
    public String toString() {
        String msg = "The animal's name is " + animalName + ".Age is " +
                this.age + ".\n";
        return msg;
    }
}
