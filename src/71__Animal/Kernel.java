import java.util.Scanner;

public class Kernel {
    public static void main (String[] args)
    {
        Scanner scan = new Scanner(System.in);

        System.out.println("Input name :");
        String name = scan.nextLine();

        System.out.println("Input age: ");
        int age = scan.nextInt();

        Animal animal = new Animal(name, age);

        System.out.println(animal.toString());
    }
}
