import java.util.Scanner;
import java.util.Random;

public class noName
{
	public static void main(String[] args)
	{
		Scanner scan=new Scanner(System.in);
		Random rand=new Random();
		
		int machine,user;
		int win=0,lose=0,draw=0;
		System.out.println("Make your choice(q to quit):");
		System.out.println("r for rock\np for paper\ns for scissors");
		
		String temp=scan.nextLine();
		char userChoice=temp.charAt(0);
		while(userChoice!='q')
		{
			switch(userChoice)
			{
				case 'r':
					user=0;
					break;
				case 's':
					user=1;
					break;
				case 'p':
					user=2;
					break;
				default:
					System.out.println("You enter wrong character.");
					System.out.println("Make your choice(q to quit):");
					System.out.println("r for rock\np for paper\ns for scissors");
					temp=scan.nextLine();
					userChoice=temp.charAt(0);
					continue;
			}
		
			machine=rand.nextInt(3);
		
			if(machine==0)
			{
				if(user==0)
				{
					System.out.println("Draw Game!");
					draw++;
				}
				if(user==1)
				{
					System.out.println("You Lose!");
					lose++;
				}
				if(user==2)
				{
					System.out.println("You Win!");
					win++;
				}
			}
			if(machine==1)
			{
				if(user==0)
				{
					System.out.println("You Win!");
					win++;
				}
				if(user==1)
				{
					System.out.println("Draw Game!");
					draw++;
				}
				if(user==2)
				{
					System.out.println("You Lose!");
					lose++;
				}
			}
			if(machine==2)
			{
				if(user==0)
				{
					System.out.println("You Lose!");
					lose++;
				}
				if(user==1)
				{
					System.out.println("You Win!");
					win++;
				}
				if(user==2)
				{
					System.out.println("Draw Game!");
					draw++;
				}
			}
			
			System.out.println("Make your choice(q to quit):");
			System.out.println("r for rock\np for paper\ns for scissors");
			temp=scan.nextLine();
			userChoice=temp.charAt(0);
		}
		System.out.println("You win "+win+" times\nYou lose "+lose+" times\nDraw game "+draw+" times");
	}
}