//************************************************************
//  Advice.java       Java Foundations
//
//  Represents some thoughtful advice.Used to demonstrates the use
//  of an overriden method.
//************************************************************
public class Advice {
    //--------------------------------------------------------
    //  Prints a message. This method overrides the parent's version.
    //--------------------------------------------------------
    public void message()
    {
        System.out.println("Warning: Dates in calendar are closer "+
            "than they appear.");

        System.out.println();

        super.message();
    }
}
