import java.util.Scanner;

public class PP4_1
{
	public static void main(String[] args)
	{
		Scanner scan=new Scanner(System.in);
		
		System.out.println("Enter a year: ");
		int year=scan.nextInt();
		
		if(year<1582)
			System.out.println("Wrong year entered.");
		else if(year%100==0)
		{
			if(year%400==0)
				System.out.println("BINGO!");
		}
		else if(year%4==0)
			System.out.println("BINGO!");
		else
			System.out.println("TRY LATER!");
	}
}

		