//****************************************************
//
//	Stars.java		Java Foundations
//
//	Demonstrates the use of nested for loops.
//
//****************************************************

public class PP4_10b
{
	//------------------------------------------------
	//
	//	Prints a triangle shape using asterisk (star)
	//	characters.
	//
	//------------------------------------------------
	public static void main(String[] args)
	{
		final int MAX_ROWS=10;
		
		for(int row=10;row>=1;row--)
		{
			int star=1;
			
			for(;star<row;star++)
				System.out.print(" ");
			
			while(star<=MAX_ROWS)
			{
				System.out.print("*");
				star++;
			}
			
			System.out.println();
		}
	}
}