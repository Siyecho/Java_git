import java.util.Scanner;

public class Money
{
	public static void main(String[] args)
	{
		System.out.print("Input a number in double: ");
		
		Scanner scan = new Scanner(System.in);
		double doublemoney=scan.nextDouble();
		
		int money=(int)(doublemoney*100);
		
		System.out.println(money);
		
		System.out.println((money/5000)+" 50Yuan");
		
		money%=5000;
		System.out.println((money/2000)+" 20Yuan");
		
		money%=2000;
		System.out.println((money/1000)+" 10Yuan");
		
		money%=1000;
		System.out.println((money/500)+" 5Yuan");
		
		money%=500;
		System.out.println((money/100)+" 1Yuan");
		
		money%=100;
		System.out.println((money/50)+" 5dime");
		
		money%=50;
		System.out.println((money/10)+" 1dime");
		
		money%=10;
		System.out.println((money/5)+" 5cent");
		
		money%=5;
		System.out.println((money/2)+" 2cent");
		
		money%=2;
		System.out.println((money/1)+" 1cent");
	}
}