package com;
import java.io.*;

import android.mediautil.image.jpeg.LLJTran;
import android.mediautil.image.jpeg.LLJTranException;
import android.mediautil.image.jpeg.*;
import android.util.Log;
import android.os.Environment;

import static android.mediautil.image.jpeg.AbstractImageInfo.NA;

public class PhotoPrivacy
{
public static final String TAG = "sunzhe";
    /**
     * 主函数，测试调用的函数接口
     *
     * @param
     */
    public static void start() throws Exception{
        Log.i(TAG, "start: ");
        String photopath = Environment.getExternalStorageDirectory().getAbsolutePath();
        writePicExif(photopath + "/data/test4.jpg", photopath + "/data/test5.jpg");
        String a = readPicExif(photopath + "/data/test5.jpg");
        System.out.println(a);
    }



    //private static void writePicExif(String inPicFileName, String outPicFileName1, String outPicFileName2)throws Exception
    private static void writePicExif(String inPicFileName, String outPicFileName)throws Exception
    {

        //原文件
        InputStream fip = new BufferedInputStream(new FileInputStream(inPicFileName)); // No need to buffer
        LLJTran llj = new LLJTran(fip);
        Log.i(TAG, "start2: ");
        try {
            llj.readSJPG(LLJTran.READ_INFO, true);
            //用SJPG版本的读取EXIF接口，对没有隐私标记的照片，增加隐私标记，对已有隐私标记
        } catch (LLJTranException e) {
            e.printStackTrace();
        }

        Exif exif = (Exif) llj.getImageInfo(); //提取文件中的EXIF数据存为EXIF数据结构
        Entry e; //新建EXIF格式的数据值

/*        // Set some values directly to gps IFD
        //设置具体的纬度
        e = new Entry(Exif.ASCII);
        e.setValue(0, "W");
        exif.setTagValueSJPG(Exif.GPSLatitudeRef,Exif.GPSINFO, e, true);

        e = new Entry(Exif.RATIONAL);
        e.setValue(0, new Rational(12, 1));
        e.setValue(1, new Rational(21, 1));
        e.setValue(2, new Rational(3, 1));
        exif.setTagValueSJPG(Exif.GPSLatitude,Exif.GPSINFO, e, true);

        //设置具体的经度
        e = new Entry(Exif.ASCII);
        e.setValue(0, "S");
        exif.setTagValueSJPG(Exif.GPSLongitudeRef,Exif.GPSINFO, e, true);

        e = new Entry(Exif.RATIONAL);
        e.setValue(0, new Rational(11, 1));
        e.setValue(1, new Rational(58, 1));
        e.setValue(2, new Rational(11, 1));
        exif.setTagValueSJPG(Exif.GPSLongitude,Exif.GPSINFO, e, true);*/

        e = exif.getTagValue(Exif.UplouderId, Exif.PRIVACY, true);
        String a = e.toString();

        if(a.equals("nsp_privacy_data")){
            // Set Privacy
            e = new Entry(Exif.ASCII);
            //这里填写手机唯一识别码的字符串
            e.setValue(0, "new uploader");

            exif.setTagValueSJPG(Exif.UplouderId,Exif.PRIVACY, e, true);

            e = new Entry(Exif.ASCII);
            //这里填写手机唯一识别码的字符串
            e.setValue(0, "2018-04-16 20:31:21");

            exif.setTagValueSJPG(Exif.TimeOfTaken,Exif.PRIVACY, e, true);

        // Set some values directly to PRIVACY IFD
        //设置具体的纬度
 /*       e = new Entry(Exif.ASCII);
        e.setValue(0, "W");
        exif.setTagValueSJPG(Exif.LatitudeRef,Exif.PRIVACY, e, true);

        e = new Entry(Exif.RATIONAL);
        e.setValue(0, new Rational(12, 1));
        e.setValue(1, new Rational(21, 1));
        e.setValue(2, new Rational(3, 1));
        exif.setTagValueSJPG(Exif.Latitude,Exif.PRIVACY, e, true);

        //设置具体的经度
        e = new Entry(Exif.ASCII);
        e.setValue(0, "S");
        exif.setTagValueSJPG(Exif.LongitudeRef,Exif.PRIVACY, e, true);

        e = new Entry(Exif.RATIONAL);
        e.setValue(0, new Rational(11, 1));
        e.setValue(1, new Rational(58, 1));
        e.setValue(2, new Rational(11, 1));
        exif.setTagValueSJPG(Exif.Longitude,Exif.PRIVACY, e, true);*/

        }else{
            int TraceTag = 1;
            while(true){
                e = exif.getIFD_SJPG(TraceTag, Exif.TRACE, true);

                if(e == null) {
                    Entry result = null;
                    result = exif.getIFD_SJPG(Exif.TRACE, true);

                    if (result == null) {
                        exif.addIFD_SJPG(Exif.TRACE, true);
                        //记录单条记录
                    }
                    break;
                }
                TraceTag++;
            }

            exif.addIFD_SJPG(TraceTag, Exif.TRACE, true);
            e = new Entry(Exif.ASCII);
            //这里填写手机唯一识别码的字符串
            e.setValue(0, "SUNZHE ANDROID");
            exif.setTagValueSJPG(01, TraceTag, Exif.TRACE, e, true);

        }



        llj.refreshAppx(); // Recreate Marker Data for changes done

        //改写后的文件，文件必须存在
        File outfile = new File(outPicFileName);
        //InputStream in = new BufferedInputStream(new FileInputStream(outPicFileName1));
        OutputStream out = new BufferedOutputStream(new FileOutputStream(outfile));

        // Transfer remaining of image to output with new header.
        //llj.xferInfo(in, out, LLJTran.REPLACE, LLJTran.REPLACE);
        llj.xferInfo(null, out, LLJTran.REPLACE, LLJTran.REPLACE);
        fip.close();
        out.close();

        llj.freeMemory();
    }

    private static String readPicExif(String inPicFileName)throws Exception
    {

        //原文件
        InputStream fip = new BufferedInputStream(new FileInputStream(inPicFileName)); // No need to buffer
        LLJTran llj = new LLJTran(fip);
        try {
            llj.readSJPG(LLJTran.READ_INFO, true);
        } catch (LLJTranException e) {
            e.printStackTrace();
        }

        Exif exif = (Exif) llj.getImageInfo();
        //Entry e = exif.getTagValue(Exif.PrivacyPath, true);
        // Set some values directly to gps IFD
//        Entry e = exif.getTagValue(Exif.PRIVACY, true);
//        if (e != null)
//            return e.toString();


        fip.close();


        llj.freeMemory();
        return NA;
    }

}