public class Dictionaries2 {
    private int definitions;

    //--------------------------------------------------------
    //  Constructor: Sets up the dictionary with the specified number
    //  of pages and definitions.
    //--------------------------------------------------------
    public Dictionaries2 (int numPages, int numDefinitions)
    {
        super(numPages);

        definitions = numDefinitions;
    }

    //--------------------------------------------------------
    //  Prints a message using both local and inherited values.
    //--------------------------------------------------------
    public double computeRatio()
    {
        return definitions/pages;
    }

    //--------------------------------------------------------
    //  Definitions mutator.
    //--------------------------------------------------------
    public void setDefinitions(int numDefinitions)
    {
        definitions = numDefinitions;
    }

    //--------------------------------------------------------
    //  Definitions accessor.
    //--------------------------------------------------------
    public int getDefinitions()
    {
        return definitions;
    }
}
