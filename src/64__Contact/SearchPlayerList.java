public class SearchPlayerList {
    public static void main (String[] args)
    {
        Contact[] players = new Contact[7];

        players[0] = new Contact("Rodger", "Federer", "610");
        players[1] = new Contact("Andy", "Roddick", "215");
        players[2] = new Contact("Maria", "Sharapova", "733");
        players[3] = new Contact("Venus", "Williams", "663");
        players[4] = new Contact("Lleyton", "Hewitt", "464");
        players[5] = new Contact("Eleni", "Dani", "322");
        players[6] = new Contact("Serena", "Willi", "242");

        Contact target = new Contact("Eleni", "Dani", " ");

        Contact found = (Contact)Searching.linearSearch(players, target);

        if (found == null)
            System.out.println("Player was not found");
        else
            System.out.println("Found: " + found);
    }
}
