//************************************************************
//
//	LightBulbPanel.java			Java Foundations
//
//	Represents the image for the LightBulb program.
//
//************************************************************

import javax.swing.*;
import java.awt.*;

public class LightBulbPanel extends JPanel
{
	private boolean on;
	private ImageIcon lightOn,lightOff;
	private JLable imageLable;
	
	//--------------------------------------------------------
	//	Constructor: Sets up the images and the initial state.
	//--------------------------------------------------------
	
	public LightBulbPanel()
	{
		lightOn = new ImageIcon ("lightBulbOn.gif");
		lightOff = new ImageIcon ("lightBulbOff.gif");
		
		setBackground (Color.black);
		
		on = true;
		imageLable = new JLabel (lightOff);
		add (imageLable);
	}
	
	//--------------------------------------------------------
	//	Paints the panel using the appropriate image.
	//--------------------------------------------------------
	
	public void paintComponent (Graphics page)
	{
		super.paintComponent(page);
		
		if (on)
			imageLable.setIcon (lightOn);
		else
			imageLable.setIcon (lightOff);
	}
	
	//--------------------------------------------------------
	//	Sets the status of the light bulb.
	//--------------------------------------------------------
	
	public void setOn (boolean lightBulbOn)
	{
		on = lightBulbOn;
	}
}