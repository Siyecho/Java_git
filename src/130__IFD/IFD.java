
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class IFD extends Entry {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8866775011364812679L;
	public IFD(int tag) {
		this(tag, Exif.UNDEFINED);
	}
	
	public IFD(int tag, int type) {
		super(type);
		this.tag = tag;
		entries = new TreeMap<Integer, Entry>();
	}
	
	public void addEntry(int tag, Entry entry) {
		entries.put(tag, entry);
	}
	
	public void removeEntry(int tag) {
		entries.remove(tag);
	}

	public void addIFD(IFD ifd) {
		IFD[] temp = ifds==null?new IFD[1]:new IFD[ifds.length+1];
		temp[ifds==null?0:ifds.length] = ifd;
		if (ifds != null)
			System.arraycopy(ifds, 0, temp, 0, ifds.length);
		ifds = temp;
	}
	
	public Entry getEntry(int tag, int subTag) {
		Entry result = entries.get(tag);
		if (result != null)
			return result;
		if (subTag > 0) {
			for (int i=0; i<ifds.length; i++)
				if (ifds[i].getTag() == subTag)
					return ifds[i].getEntry(tag, -1);
		} else {
			for (int i=0; ifds != null && i<ifds.length; i++) {
				result = ifds[i].getEntry(tag, -1);
				if (result != null)
					break;
			}
		}
		return result;
	}
	
	public IFD getIFD(int tag) {
		for (int i=0; i<ifds.length; i++)
			if (ifds[i].getTag() == tag)
				return ifds[i];
		return null;
	}

	public IFD getIFD_SJPG(int tag, int subtag) {
		for (int i=0; i<ifds.length; i++)
			if (ifds[i].getTag() == subtag)
			{
				return ifds[i].getIFD(tag);
			}
		return null;
	}

	public int getTag() {
		return tag;
	}
	
	public Entry setEntry(int tag, int subTag, Entry value) {
		Entry result = null;
		if (subTag > 0) {
			for (int i=0; i<ifds.length; i++)
				if (ifds[i].getTag() == subTag)
					return ifds[i].setEntry(tag, -1, value);
		} else if (subTag == 0) {
			result = entries.put(tag, value);
		} else {
			for (int i=0; i<ifds.length; i++) {
				result = ifds[i].getEntry(tag, -1);
				if (result != null) {
					ifds[i].setEntry(tag, 0, value);
					break;
				}
			}
		}
		return result;
	}

	public Entry setEntrySJPG(Integer tag, int subTag, Entry value) {
		Entry result = null;
		if (subTag > 0) {
			for (int i=0; i<ifds.length; i++)
				if (ifds[i].getTag() == subTag)
					return ifds[i].setEntrySJPG(tag, 0, value);
		} else if (subTag == 0) {
			result = (Entry)entries.put(tag, value);
		} else {
			for (int i=0; i<ifds.length; i++) {
				result = ifds[i].getEntry(tag, -1);
				if (result != null) {
					ifds[i].setEntrySJPG(tag, 0, value);
					break;
				}
			}
		}
		return result;
	}

	public Entry setEntrySJPG(Integer tag, int subTag, int fatherTag, Entry value) {
		Entry result = null;
		if (fatherTag > 0) {
			for (int i=0; i<ifds.length; i++)
				if (ifds[i].getTag() == fatherTag)
					return ifds[i].setEntrySJPG(tag, subTag, value);
		} else if (subTag == 0) {
			result = (Entry)entries.put(tag, value);
		} else {
			for (int i=0; i<ifds.length; i++) {
				result = ifds[i].getEntry(tag, -1);
				if (result != null) {
					ifds[i].setEntrySJPG(tag, 0, value);
					break;
				}
			}
		}
		return result;
	}

	public void addIFDSJPG(Integer tag, int subTag) {
		for (int i=0; i<ifds.length; i++){
			if (ifds[i].getTag() == subTag) {
				IFD iifd_SJPG;
				iifd_SJPG = new IFD(tag, 4);
				ifds[i].addIFD(iifd_SJPG);
			}
		}
	}

	public Map<Integer, Entry> getEntries() {
		return entries;
	}
	
	public IFD[] getIFDs() {
		return ifds;
	}

	protected SortedMap<Integer, Entry> entries;
	protected IFD[] ifds;
	protected int tag;
}
