import java.util.Date;
import java.util.Scanner;

public class Bookshelf {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);

        String name = scan.nextLine();
        String author = scan.nextLine();
        String editor = scan.nextLine();
        Date date = new Date();

        Book book = new Book(name, author, editor, date);

        String msg = book.toString();

        System.out.println(msg);
    }
}
