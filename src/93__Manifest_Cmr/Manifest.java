package com.example.user.ez_camera;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.user.camera_usingsystemcamera.R;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    Uri imgUri;
    ImageView imv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        imv = (ImageView)findViewById(R.id.image);
    }

    public void onGet(View v){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},200);
        }
        else {
            savePhoto();
        }
        //Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        //startActivityForResult(it,100);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case 100:
                    Intent it = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, imgUri);
                    sendBroadcast(it);
                    break;
                case 101:
                    //imgUri = convertUri(data.getData());
                    break;
            }
            showImg();
/*
        if (resultCode == Activity.RESULT_OK && requestCode ==100){

            showImg();
           -------------------------------------------------------------------------------------------
            Bitmap bmp = null;
            try {
              bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(imgUri),null,null);
            }catch (IOException e){
                Toast.makeText(this, "无法读取照片", Toast.LENGTH_LONG).show();
            }
            Bundle extras = data.getExtras();
            Bitmap bmp = (Bitmap) extras.get("data");
            ImageView imv = (ImageView)findViewById(R.id.imageView);
            imv.setImageBitmap(bmp);
            -------------------------------------------------------------------------------------------
            */
        }
        else {
            Toast.makeText(this, requestCode == 100? "没有拍到照片":"没有选取照片", Toast.LENGTH_LONG).show();
            //Toast.makeText(this, "没有拍到照片", Toast.LENGTH_LONG).show();
        }
    }

    public void onPick(View v){
        Intent it = new Intent(Intent.ACTION_GET_CONTENT);
        it.setType("image/*");
        startActivityForResult(it, 101);
    }

    void showImg() {

        int iw, ih, vw, vh;
        boolean needRotate;

        BitmapFactory.Options option = new BitmapFactory.Options();

        option.inJustDecodeBounds = true;

        try {
            BitmapFactory.decodeStream(getContentResolver().openInputStream(imgUri), null, option);
        }
        catch (IOException e){
            Toast.makeText(this,"读取照片时发生错误", Toast.LENGTH_LONG).show();
            return;
        }
        iw = option.outWidth;
        ih = option.outHeight;
        vw = imv.getWidth();
        vh = imv.getHeight();

        //int scaleFactor = Math.min(iw/vw, ih/vh);
        int scaleFactor;
        if (iw < ih){
            needRotate = false;
            scaleFactor = Math.min(iw/vw, ih/vh);
        }
        else {
            needRotate = true;
            scaleFactor = Math.min(ih/vw, iw/vh);
        }

        option.inJustDecodeBounds = false;
        option.inSampleSize = scaleFactor;

        Bitmap bmp = null;
        try {
            bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(imgUri), null, option);
        }catch (IOException e){
            Toast.makeText(this, "无法取得照片", Toast.LENGTH_LONG).show();
        }

        if (needRotate){
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            bmp = Bitmap.createBitmap(bmp,340, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
        }

        imv.setImageBitmap(bmp);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        if (requestCode == 200){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                savePhoto();
            }
            else{
                Toast.makeText(this, "程序需要写入权限才能运行", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void savePhoto(){
        imgUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new ContentValues());
        Intent it = new Intent("android.media.action.IMAGE_CAPTURE");
        it.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);

        startActivityForResult(it, 100);
    }
}
