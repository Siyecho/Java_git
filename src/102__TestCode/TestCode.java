package mediautil.test;

import java.io.*;
import java.util.Random;

import mediautil.gen.directio.IterativeReader;
import mediautil.gen.directio.SplitInputStream;

class TestCode implements IterativeReader
{
    public static byte fileArr[];
    public static Random r;
    public static int id;
    private InputStream ip;
    private int pos, n, skipProb, maxBuf, maxReads;
    private PrintStream refOp, sipOp;
    private byte readBuf[];
    public static boolean flush = false;

    public static void printBytes(PrintStream op, byte b[], int off, int len)
    {
        while(len > 0)
        {
            op.println("" + b[off]);
            off++;
            len--;
        }
    }

    public TestCode(InputStream ip, int n, int skipProb, int maxBuf,
                    int maxReads) throws FileNotFoundException
    {
        this.ip = ip;
        this.n = n;
        this.skipProb = skipProb;
        this.maxBuf = maxBuf;
        this.maxReads = maxReads;
        pos = 0;
        readBuf = new byte[maxBuf];
        refOp = new PrintStream(new BufferedOutputStream(new FileOutputStream("sub" + n + "_ref.txt"), 100000));
        sipOp = new PrintStream(new BufferedOutputStream(new FileOutputStream("sub" + n + "_sip.txt"), 100000));
    }

public static boolean debug = false;

    private boolean prevShort = false;
    public int nextRead(int numBytes)
    {
        int readCount = r.nextInt(maxReads) + 1;
        int len = 0, ofs, actualRead;
        boolean isSkip = false;
        int retVal = IterativeReader.CONTINUE;
      try {
        do
        {
            len = r.nextInt(maxBuf) + 1;
            isSkip = r.nextInt(16) < skipProb;
            String opMsg = isSkip?"Skipped ":"Read ";
            refOp.println("Trying Id: " + id + ' ' + opMsg + pos + " - " + (pos+len));
            sipOp.println("Trying Id: " + id + ' ' + opMsg + pos + " - " + (pos+len));
            if(flush)
            {
                refOp.flush();
                sipOp.flush();
            }

            if(isSkip)
                actualRead = (int)ip.skip(len);
            else {
                ofs = TestCode.r.nextInt(maxBuf-len+1);
                actualRead = ip.read(readBuf, ofs, len);
                printBytes(refOp, TestCode.fileArr, pos, actualRead);
                printBytes(sipOp, readBuf, ofs, actualRead);
            }
            if(actualRead > 0)
            {
                if(actualRead < len)
                    prevShort = true;
                refOp.println("Id: " + id + " readCount: " + readCount + ' ' + opMsg + pos + " - " + (pos+actualRead));
                sipOp.println("Id: " + id + " readCount: " + readCount + ' ' + opMsg + pos + " - " + (pos+actualRead));
            }
            else
            {
                refOp.println("Id: " + id + " readCount: " + readCount + ' ' + " End Of Stream actualRead = " + actualRead);
                sipOp.println("Id: " + id + " readCount: " + readCount + ' ' + " End Of Stream actualRead = " + actualRead);
            }
            if(flush)
            {
                refOp.flush();
                sipOp.flush();
            }
            if(actualRead > 0)
                pos += actualRead;
            --readCount;
        } while(actualRead >= 0 && readCount > 0);
        if(actualRead < 0)
        {
            retVal = IterativeReader.STOP;
            refOp.close();
            sipOp.close();
        }
      } catch(Exception e)
      {
        e.printStackTrace(System.err);
        sipOp.println("Exception for Id: " + id + " isSkip = " + isSkip + " readCount: " + readCount + ' ' + " Len = " + len);
        e.printStackTrace(sipOp);
        sipOp.flush();
        throw new RuntimeException("What the Heaven");
      }
        return retVal;
    }

    public void closeFiles()
    {
        refOp.close();
        sipOp.close();
        refOp = null;
        sipOp = null;
    }
}

