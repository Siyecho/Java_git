public class encryptAndDecrypt {
	void getsubreshuffle(int[][]pixels,byte[] pkey) throws NoSuchAlgorithmException, InvalidKeyException
    {
        int [][][] subpix = new int[wwnum * hhnum][snum * snum][subblocksize * subblocksize];
        int i,j,k,q,p;
        long seed;
        for (i = 0; i < wnum; i++)
        {
            for(j = 0; j < hnum; j++)
            {
                for(k = 0; k < snum * snum ;k++)
                {
                    int m = -1;
                    for(q = 0; q < subblocksize; q++)
                    {
                        for(p = 0; p < subblocksize; p++)
                        {
                            subpix[i*hhnum+j][k][++m]=pixels[i*hhnum+j][((k/snum)*subblocksize+q)*blocksize+(k%snum)*subblocksize+p];
                        }
                    }
                }
            }
        }
        
        for (i = 0; i < wnum; i++)
        {
            for(j = 0; j < hnum; j++)
            {
                for(k =0; k < snum; k++)
                {
                    for(q = 0; q < snum;q++)
                    {
                        seed = hmac_inpic(k,q,pkey);
                        //for(k = 0;k < 3; k++)
                        //{
                        refisherandyates(subpix[i * hhnum + j][k*snum+q],seed);
                    }
                }
            }
        }
        for (i = 0; i < wnum; i++)
        {
            for(j = 0; j < hnum; j++)
            {
                seed = hmac_inpic(i,j,pkey);
                //for(k = 0;k < 3; k++)
                //{
                subrefisherandyates(subpix[i * hhnum + j],seed);
                
            }
        }
        for (i = 0; i < wnum; i++)
        {
            for(j = 0; j < hnum; j++)
            {
                for(k = 0; k < snum * snum ;k++)
                {
                    int m = -1;
                    for(q = 0; q < subblocksize; q++)
                    {
                        for(p = 0; p < subblocksize; p++)
                        {
                            pixels[i*hhnum+j][((k/snum)*subblocksize+q)*blocksize+(k%snum)*subblocksize+p]=subpix[i*hhnum+j][k][++m];
                        }
                    }
                }
            }
        }
        //return subpix;
    }
    
    //进行加密
    public void encrypt() throws InvalidKeyException, NoSuchAlgorithmException, IOException
    {
        Key akey = new Key(filename1);
        akey.getkey(0,1);
        byte[] ykey,ukey,vkey;
        //ykey = akey.getthreekeys("Y");
        //ukey = akey.getthreekeys("U");
        vkey = akey.getthreekeys("V");
        
        int[][] pixels = new int[wwnum * hhnum][];// = getplane();
        
            //int i, j, R, G, B, Y, U, V;
        
        pixels = getplane();
        
        if(subblocksize != 0)
        {
            getsubshuffle(pixels,vkey);
            //savefile(pixels);
        }
        else
        {
            //int[][][]pixels = new int[wwnum * hhnum][snum * snum][];
            //pixels = get
            pixels = shuddle(pixels,vkey);
            
        }
        savefile(pixels);//
    }
    
    //进行解密
    public void decrypt() throws InvalidKeyException, NoSuchAlgorithmException, IOException
    {
        Key akey = new Key(filename1);
        akey.getkey(0,1);
        byte[] ykey,ukey,vkey;
        //ykey = akey.getthreekeys("Y");
        //ukey = akey.getthreekeys("U");
        vkey = akey.getthreekeys("V");
        
        int[][] pixels = new int[wwnum * hhnum][];// = getplane();
        
        //int i, j, R, G, B, Y, U, V;
        
        pixels = getplane();
        
        if(subblocksize!=0)
        {
            getsubreshuffle(pixels,vkey);
        }
        else
        {
            reshuddle(pixels,vkey);
        }
        savefile(pixels);//
    }
}
	}