//************************************************************
//	LeftRight.java		Java Foundations
//
//	Demonstrates the use of one listener for multiple buttons.
//************************************************************

import javax.swing.JFrame;

public class LeftRight
{
	//--------------------------------------------------------
	//	Create and displays the main program frame.
	//--------------------------------------------------------
	public static void main(String[] args)
	{
		//	JFrame的构造方法带有一个字符串类型的参数，框架的标题栏
		//	将显示这个字符串	
		JFrame frame = new JFrame ("Left Right");
		
		//	确定点击关闭按钮时执行什么操作
		frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		
		//	getContentPane得到框架的内容窗格
		//	add将面板添加到内容窗格中
		frame.getContentPane().add(new LeftRightPanel());
		
		//	pack将根据框架中的内容把框架设置为合适的尺寸
		frame.pack();
		
		//	在屏幕上显示框架
		frame.setVisible(true);
	}
}