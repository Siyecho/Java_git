public class CameraUtils {
	private static ProgressDialog dialog;

	/**
	 * 判断外部存储设备的可用性
	 *
	 * @return 外部设备是否可用
	 */
	public static boolean isExternalStorageUseful() {
		boolean bl = false;
		// 调用Environment中的静态方法getExternalStorageState()获取sdcard的状态
		String state = Environment.getExternalStorageState();
		// 将获取sdcard的状态与MEDIA_MOUNTED(可读可写)常量比较
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			bl = true;
		}
		return bl;
	}



	/**
	 * 向sdcard根目录写入文件
	 *
	 * @param fileName
	 *            文件名称
	 * @param content
	 *            文件内容
	 * @return 是否写入成功 getExternalStorageDirectory()获取sdcard根目录文件对象
	 */
	public static boolean writeSdcardRootDir(String fileName, byte[] content) {

		boolean bl = false;
		if (isExternalStorageUseful()) {
			FileOutputStream outputStream = null;
			try {
				File parentFile = Environment.getExternalStorageDirectory();// mnt/sdcard/filename
				Log.i("parentFile", "*************" + parentFile);
				File file = new File(parentFile, fileName);
				outputStream = new FileOutputStream(file);
				outputStream.write(content);
				bl = true;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (outputStream != null) {
					try {
						outputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

		}

		return bl;
	}

	/**
	 * 图片旋转90并存储
	 * @param bitmap
	 * @param fileName
	 * @return
	 */

	public static boolean saveBitmap(Bitmap bitmap,String fileName){
		boolean isOK=false;
		/*dialog = new ProgressDialog(context);
		dialog.setMessage("正在保存请稍后...");
		dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		dialog.show();*/
		try {
			File parentFile = Environment.getExternalStorageDirectory();// mnt/sdcard/filename
			Log.i("parentFile", "*************" + parentFile);
			File file = new File(parentFile, fileName);

			FileOutputStream fout = new FileOutputStream(file.getPath());
			BufferedOutputStream bos = new BufferedOutputStream(fout);

			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
			isOK = true;
			bos.flush();
			bos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
//		dialog.dismiss();
		return isOK;
	}


	/**
	 * 在sdcard根目录下读取文件
	 *
	 * @param fileName
	 *            文件名称
	 * @return 读取的文件内容
	 */
	public static byte[] readSdcardRootDir(String fileName) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		if (isExternalStorageUseful()) {
			FileInputStream inputStream = null;
			try {
				File parentFile = Environment.getExternalStorageDirectory();
				File file = new File(parentFile, fileName);
				inputStream = new FileInputStream(file);
				int temp = 0;
				byte[] buff = new byte[1024];
				while ((temp = inputStream.read(buff)) != -1) {
					outputStream.write(buff, 0, temp);
					outputStream.flush();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (inputStream != null) {
					try {
						inputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return outputStream.toByteArray();
	}

	/**
	 * 图片旋转
	 * @param b
	 * @param rotateDegree
	 * @return
	 */

	public static Bitmap getRotateBitmap(Bitmap b, float rotateDegree){
		Matrix matrix = new Matrix();
		matrix.postRotate((float)rotateDegree);
		Bitmap rotaBitmap = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, false);
		return rotaBitmap;
	}

	/**
	 * 获取屏幕尺寸
	 * @param context
	 * @return
	 */
	public static Point getScreenMetrics(Context context){
		DisplayMetrics dm =context.getResources().getDisplayMetrics();
		int w_screen = dm.widthPixels;
		int h_screen = dm.heightPixels;
		return new Point(w_screen, h_screen);

	}

	/**
	 * 获取屏幕密度
	 * @param context
	 * @return
	 */
	public static float getScreenRate(Context context){
		Point P = getScreenMetrics(context);
		float H = P.y;
		float W = P.x;
		return (H/W);
	}
}
