package javafoundations;

import javafoundations.exceptions.*;

public class LinkedStack<T> implements Stack<T> {
    private int count;
    private LinearNode<T> top;

    public LinkedStack()
    {
        count = 0;
        top = null;
    }

    public T pop() throws EmptyCollectionException
    {
        if (count == 0)
            throw new EmptyCollectionException ("Pop operation failed. "
            + "The stack is empty.");
        T result = top.getElement();
        top = top.getNext();
        count--;

        return result;
    }

    @Override
    public String toString() {
        String result = "<top of stack>\n";
        LinearNode current = top;

        while (current != null)
        {
            result += current.getElement() + "\n";
            current = current.getNext();
        }

        return result + "<bottom of stack>";
    }
}
