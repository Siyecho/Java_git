import java.util.Date;

public class Book {
    private String name;
    private String author;
    private String editor;
    private Date date;

    public Book(String name, String author, String editor, Date date)
    {
        this.name = name;
        this.author = author;
        this.editor = editor;
        this.date = date;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public void setEditor(String editor)
    {
        this.editor = editor;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public String getName()
    {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getEditor() {
        return editor;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Name" + "\t\t" + "Author" + "\t\t" + "Editor" + "\t\t" + "Date" + "\n"
                + name + "\t" + author + "\t" + editor + "\t" + date + "\n";
    }
}
