public class Box {
    private double high;
    private double width;
    private double deepth;
    private boolean full = false;

    public Box(double tall, double wid, double deep)
    {
        high = tall;
        width = wid;
        deepth = deep;
    }

    public void setHigh(double tall)
    {
        high = tall;
    }

    public double getHigh()
    {
        return high;
    }

    public void setWidth(double wid)
    {
        width = wid;
    }

    public double getWidth()
    {
        return width;
    }

    public void setDeepth(double deep)
    {
        deepth = deep;
    }

    public double getDeepth()
    {
        return deepth;
    }

    public void setFull(boolean empty)
    {
        full = empty;
    }

    public boolean isFull()
    {
        return full;
    }

    @Override
    public String toString() {
        return "The high is " + high + ".\n"
                + "The width is " + width + ".\n"
                +"The deepth is " + deepth +".\n";
    }
}
