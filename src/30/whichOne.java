//**********************************************************
//	PalindromeTester.java		Java Foundations
//
//	Demonstrates the use of nested while loops.
//**********************************************************

import java.util.Scanner;

public class whichOne
{
	//------------------------------------------------------
	//	Tests strings to see if they are palindromes.
	//------------------------------------------------------
	public static void main(String[] args)
	{
		String str,another="y";
		int left,right;
		
		Scanner scan=new Scanner(System.in);
		while(another.equalsIgnoreCase("y"))
		{
			System.out.println("Enter a potential palindrome: ");
			str=scan.nextLine();
			
			left=0;
			right=str.length()-1;
			char charLeft,charRight;
			
			while(left<right)
			{
				charLeft=str.charAt(left);
				charRight=str.charAt(right);
				
				if(charLeft==' '||charLeft<='/'
					||(charLeft>='['&&charLeft<='\'')
					||charLeft>='{')
				{
					left++;
					continue;
				}
				
				if(charRight==' '||charRight<='/'
					||(charRight>='['&&charRight<='\'')
					||charRight>='{')
				{
					right--;
					continue;
				}
				
				if(charLeft==charRight)
				{
					left++;
					right--;
				}
				else
					break;
			}
			System.out.println();
			if(left<right)
				System.out.println("That string is NOT a palindrome.");
			else
				System.out.println("That string IS a palindrome.");
			
			System.out.println();
			System.out.print("Test another palindrome (y/n)?");
			another=scan.nextLine();
		}
	}
}