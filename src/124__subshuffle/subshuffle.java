
public class subshuffle()
{
	
	void getsubshuffle(int[][]pixels,byte[] pkey) throws NoSuchAlgorithmException, InvalidKeyException
		{
			int [][][] subpix = new int[wwnum * hhnum][snum * snum][subblocksize * subblocksize];
			int i,j,k,q,p;
			long seed;
			for (i = 0; i < wnum; i++)
			{
				for(j = 0; j < hnum; j++)
				{
					for(k = 0; k < snum * snum ;k++)
					{
						int m = -1;
						for(q = 0; q < subblocksize; q++)
						{
							for(p = 0; p < subblocksize; p++)
							{
								//System.out.println("p "+p+" q "+q+ " m "+m +" i "+i+"j"+j );
								subpix[i*hhnum+j][k][++m]=pixels[i*hhnum+j][((k/snum)*subblocksize+q)*blocksize+(k%snum)*subblocksize+p];
                            
                        }
                    }
                }
            }
        }
        for (i = 0; i < wnum; i++)
        {
            for(j = 0; j < hnum; j++)
            {
                seed = hmac_inpic(i,j,pkey);
                //for(k = 0;k < 3; k++)
                //{
                subfisherandyates(subpix[i * hhnum + j],seed);
            }
        }
        
        for (i = 0; i < wnum; i++)
        {
            for(j = 0; j < hnum; j++)
            {
                for(k =0; k < snum; k++)
                {
                    for(q = 0; q < snum;q++)
                    {
                        seed = hmac_inpic(k,q,pkey);
                        //for(k = 0;k < 3; k++)
                        //{
                        fisherandyates(subpix[i * hhnum + j][k*snum+q],seed);
                        
                    }
                }
            }
        }
        
        for (i = 0; i < wnum; i++)
        {
            for(j = 0; j < hnum; j++)
            {
                for(k = 0; k < snum * snum ;k++)
                {
                    int m = -1;
                    for(q = 0; q < subblocksize; q++)
                    {
                        for(p = 0; p < subblocksize; p++)
                        {
                            pixels[i*hhnum+j][((k/snum)*subblocksize+q)*blocksize+(k%snum)*subblocksize+p]=subpix[i*hhnum+j][k][++m];
                        }
                    }
                }
            }
        }
        
        //return subpix;
    }
    
    void getsubreshuffle(int[][]pixels,byte[] pkey) throws NoSuchAlgorithmException, InvalidKeyException
    {
        int [][][] subpix = new int[wwnum * hhnum][snum * snum][subblocksize * subblocksize];
        int i,j,k,q,p;
        long seed;
        for (i = 0; i < wnum; i++)
        {
            for(j = 0; j < hnum; j++)
            {
                for(k = 0; k < snum * snum ;k++)
                {
                    int m = -1;
                    for(q = 0; q < subblocksize; q++)
                    {
                        for(p = 0; p < subblocksize; p++)
                        {
                            subpix[i*hhnum+j][k][++m]=pixels[i*hhnum+j][((k/snum)*subblocksize+q)*blocksize+(k%snum)*subblocksize+p];
                        }
                    }
                }
            }
        }
        
        for (i = 0; i < wnum; i++)
        {
            for(j = 0; j < hnum; j++)
            {
                for(k =0; k < snum; k++)
                {
                    for(q = 0; q < snum;q++)
                    {
                        seed = hmac_inpic(k,q,pkey);
                        //for(k = 0;k < 3; k++)
                        //{
                        refisherandyates(subpix[i * hhnum + j][k*snum+q],seed);
                    }
                }
            }
        }
        for (i = 0; i < wnum; i++)
        {
            for(j = 0; j < hnum; j++)
            {
                seed = hmac_inpic(i,j,pkey);
                //for(k = 0;k < 3; k++)
                //{
                subrefisherandyates(subpix[i * hhnum + j],seed);
                
            }
        }
        for (i = 0; i < wnum; i++)
        {
            for(j = 0; j < hnum; j++)
            {
                for(k = 0; k < snum * snum ;k++)
                {
                    int m = -1;
                    for(q = 0; q < subblocksize; q++)
                    {
                        for(p = 0; p < subblocksize; p++)
                        {
                            pixels[i*hhnum+j][((k/snum)*subblocksize+q)*blocksize+(k%snum)*subblocksize+p]=subpix[i*hhnum+j][k][++m];
                        }
                    }
                }
            }
        }
        //return subpix;
    }
		}