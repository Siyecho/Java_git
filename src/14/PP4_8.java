import java.util.Scanner;
import java.util.Random;

public class PP4_8
{
	public static void main(String[] args)
	{
		Scanner scan=new Scanner(System.in);
		Random rand=new Random();
		int num=rand.nextInt(100)+1;
		int time=0;
		
		System.out.print("Enter a number here(0 to quit): ");
		int guess=scan.nextInt();
		
		if(guess==0)
		{
			System.out.print("Bye!");
			System.out.println("You have tried "+time+" times.");
		}
		else
		{
			while(guess!=num)
			{
				if(guess>num)
					System.out.println("The number you guess is bigger.");
				if(guess<num)
					System.out.println("The number you guess is smaller.");
				
				System.out.print("Enter a number here(0 to quit): ");
				guess=scan.nextInt();
				time++;
				if(guess==0)
				{
					System.out.print("Bye!");
					break;
				}
			}
			System.out.println("You bat!");
			System.out.println("You have tried "+(time+1)+" times.");
		}
	}
}