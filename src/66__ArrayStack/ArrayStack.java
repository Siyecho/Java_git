package javafoundations;

import javafoundations.exceptions.*;

public class ArrayStack<T> implements Stack<T> {
    private final int DEFAULT_CAPACITY = 10;
    private int count;
    private T[] stack;

    public ArrayStack()
    {
        count = 0;
        stack = (T[]) (new Object[DEFAULT_CAPACITY]);
    }

    public void push (T element)
    {
        if (count == stack.length)
            expandCapacity();

        stack[count] = element;
        count++;
    }

    @Override
    public String toString() {
        String result = "<top of stack>\n";

        for (int index = count-1; index >=0; index--)
            result += stack[index] + "\n";

        return result + "<bottom of stack>";
    }

    private void expandCapacity()
    {
        T[] larger = (T[]) (new Object[stack.length*2]);

        for (int index = 0; index < stack.length; index++)
            larger[index] = stack[index];

        stack = larger;
    }
}
