public class SplitInputStreamTester extends SplitInputStream {

    public SplitInputStreamTester()
    {
        super(null);
    }

    public static void main(String args[]) throws Exception
    {
		
        File finfo = new File(args[0]);
        int size = (int)finfo.length();
        FileInputStream fip = new FileInputStream(finfo);
        TestCode.fileArr = new byte[size];
        int readLen = fip.read(TestCode.fileArr);
        int i;
        fip.close();
		
        // ByteArrayInputStream ip = new ByteArrayInputStream(TestCode.fileArr, 0, readLen);
        CrankyStream ip = new CrankyStream(TestCode.fileArr, 0, readLen);
        int numSubReaders = 5, skipProb = 10, maxBuf = 20;
        SplitInputStream sip = new SplitInputStream(ip, 20, 7);
        TestCode.r = new Random(555);
        i = 0;
        TestCode tc[] = new TestCode[numSubReaders];
		
        do
        {
            InputStream subIp = sip.createSubStream(10, 10);
            tc[i] = new TestCode(subIp, i, skipProb, maxBuf, 4);
            sip.attachSubReader(tc[i], subIp);
            i++;
        }
		while(i < numSubReaders);
        PrintStream refOp = new PrintStream(new BufferedOutputStream(new FileOutputStream("main_ref.txt"), 100000));
        PrintStream sipOp = new PrintStream(new BufferedOutputStream(new FileOutputStream("main_sip.txt"), 100000));
		
        boolean prevShort = false;
        int pos, len, ofs, actualRead;
        byte readBuf[] = new byte[1001];
        pos = 0;
        i = 0;
        do
        {
            len = TestCode.r.nextInt(1001) + 1;
            boolean isSkip = TestCode.r.nextInt(16) < skipProb;
            String opMsg = isSkip?"Skipped ":"Read ";
            TestCode.id = i;
            refOp.println("Trying Id: " + i + ' ' + opMsg + pos + " - " + (pos+len));
            sipOp.println("Trying Id: " + i + ' ' + opMsg + pos + " - " + (pos+len));
			
            if(TestCode.flush)
            {
                refOp.flush();
                sipOp.flush();
            }
            if(isSkip)
                actualRead = (int)sip.skip(len);
            else {
                ofs = TestCode.r.nextInt(1001-len+1);
                actualRead = sip.read(readBuf, ofs, len);
                TestCode.printBytes(refOp, TestCode.fileArr, pos, actualRead);
                TestCode.printBytes(sipOp, readBuf, ofs, actualRead);
            }
            if(actualRead > 0)
            {
                if(actualRead < len)
                    prevShort = true;
                refOp.println("Id: " + i + ' ' + opMsg + pos + " - " + (pos+actualRead));
                sipOp.println("Id: " + i + ' ' + opMsg + pos + " - " + (pos+actualRead));
            }
            else
            {
                refOp.println("Id: " + i + " End Of Stream actualRead = " + actualRead);
                sipOp.println("Id: " + i + " End Of Stream actualRead = " + actualRead);
            }
            if(TestCode.flush)
            {
                refOp.flush();
                sipOp.flush();
            }
            if(actualRead > 0)
                pos += actualRead;
            i++;
        } while (actualRead >= 0);
        refOp.close();
        sipOp.close();

        System.out.println("Max Blocks = " + sip.getMaxBufSize());
        i = 0;
        do
        {
            tc[i].closeFiles();
            tc[i] = null;
            i++;
        }while(i < numSubReaders);

        ip = null;
        TestCode.fileArr = null;
    }
}