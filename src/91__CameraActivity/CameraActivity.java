public class CameraActivity extends Activity implements View.OnClickListener {
    private Button save;
    private View mCancel, mTakePhoto;
    private SurfaceView mCameraView;
    private Camera camera;
    private boolean isShow = false;//预览状态
    private boolean isTaking = false;//拍照状态
    private SurfaceHolder mHolder;
    private float mPreviwRate = -1f;//用于设置surface比率
    private int RESULT_CODE = 321;//响应码
    private Intent intent;
    private Bitmap imageBitmap = null;//拍照获取的bitmap
    private CameraSizeComparator sizeComparator = new CameraSizeComparator();//自定义比较器

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_camer);

        initView();//UI
        initParam();


        //progress

        mHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                initCamera();
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                if (camera != null) {
                    camera.setPreviewCallback(null);
                    camera.stopPreview();
                    mPreviwRate = -1f;
                    camera.release();
                    camera = null;
                }

            }
        });

//        监听事件
        mTakePhoto.setOnClickListener(CameraActivity.this);
        mCancel.setOnClickListener(CameraActivity.this);
        save.setOnClickListener(CameraActivity.this);


    }

    private void initParam() {

        ViewGroup.LayoutParams params = mCameraView.getLayoutParams();
        //获取屏幕尺寸设置照相机全屏
        Point p = CameraUtils.getScreenMetrics(this);
        params.width = p.x;
        params.height = p.y;
        mCameraView.setLayoutParams(params);

//        设置比率
        mPreviwRate = CameraUtils.getScreenRate(this);


    }

    private void initView() {
        //初始化
        mCancel = findViewById(R.id.cancel_action);
        mTakePhoto = findViewById(R.id.take_photo);
        mCameraView = (SurfaceView) findViewById(R.id.camera_view);
        save = (Button) findViewById(R.id.save);

        //获取并设置SurfaceHolder
        mHolder = mCameraView.getHolder();
        mHolder.setFormat(PixelFormat.TRANSPARENT);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

    }

    //初始化照相机参数
    private void initCamera() {
        //打开摄像机
        if (!isShow) {
            camera = Camera.open();
        }
        //获取camera参数
        Camera.Parameters parameters = camera.getParameters();

        /**
         * 设置合适的预览大小
         */
        List<Camera.Size> previewlist = parameters.getSupportedPreviewSizes();
        Camera.Size previewSize = getPropPreviewSize(previewlist, mPreviwRate, 800);
        parameters.setPreviewSize(previewSize.width, previewSize.height);

        /**
         * 设置合适图片大小
         */
        List<Camera.Size> picturelist = parameters.getSupportedPictureSizes();
        Camera.Size pictureSize = getPropPreviewSize(picturelist, mPreviwRate, 800);
        parameters.setPreviewSize(pictureSize.width, pictureSize.height);

        parameters.setPictureFormat(ImageFormat.JPEG);//设置图片支持格式
        camera.setDisplayOrientation(90);
        /**
         *设置聚焦模式
         */
        List<String> focusModes = parameters.getSupportedFocusModes();
        if (focusModes.contains("continuous-video")) {
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
        }
        //3、设置参数
        camera.setParameters(parameters);

        //4、开启预览
        try {
            camera.setPreviewDisplay(mHolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        camera.startPreview();

    }