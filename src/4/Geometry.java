public class Geometry
{
	public static void main(String[] args)
	{
		int sides=7;
		System.out.println("A haptagon has "+sides+" sides.");
		
		sides=10;
		System.out.println("A decagon has "+sides+" sides.");
		
		sides=12;
		System.out.println("A dodecagon has "+sides+" sides.");
	}
}