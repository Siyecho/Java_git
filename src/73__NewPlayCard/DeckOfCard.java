import java.util.Random;

public class DeckOfCard {
    private String[] flower = {"Spade", "Heart", "Diamond", "Club"};
    private final int LIMIT = 52;
    private final int MaxFace = 13;
    private int index = 0, dealTemp;
    private int rest = LIMIT;

    Random random = new Random();
    Card[] cards = new Card[LIMIT];

    public DeckOfCard()
    {

    }

    public void shuffle()
    {
        for (int colorIndexNum = 0; colorIndexNum < flower.length; colorIndexNum++)
        {
            for (int number = 1; number <= MaxFace; number++)
            {
                if (index < LIMIT)
                {
                    cards[index].setColor(flower[colorIndexNum]);
                    cards[index].setPoint(number);
                    index++;
                }
            }
        }
    }

    public void deal(int cardNumber)
    {
        if ((rest-cardNumber) >= 0)
        {
            for (int i = 0; i < cardNumber; i++)
            {
                dealTemp = random.nextInt(LIMIT);
                System.out.println("The color is " + cards[dealTemp].getColor() +
                        " and the point is " + cards[dealTemp].getPoint());
            }
            rest = rest - cardNumber;
        }
    }

    public String report()
    {
        return "There is " + rest + "cards remained.\n";
    }
}
