import java.util.Scanner;

public class BoxTest {
    public static void main (String[] args)
    {
        Scanner scan = new Scanner(System.in);

        double tall = scan.nextDouble();
        double wid = scan.nextDouble();
        double deep = scan.nextDouble();

        Box gift = new Box(tall, wid, deep);

        String msg = gift.toString();

        System.out.println(msg);
    }
}
