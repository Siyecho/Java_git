public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button get;
    private Button set;
    private ExifInterface exif;
    private List<String> mlists = new ArrayList<>();
    private RecyclerView recyclerView;
    private MyAdapter adapter;
    private String path;
    private EditText edit;
    private Button set_md5;
    private boolean encode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        initData();
        initRecycler();
        initListener();
    }

    private void initView() {
        edit = (EditText) findViewById(R.id.edit);
        get = (Button) findViewById(R.id.btn_get);
        set = (Button) findViewById(R.id.btn_set);
        set_md5 = (Button) findViewById(R.id.btn_set_md5);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
    }

    private void initListener() {
        set.setOnClickListener(this);
        set_md5.setOnClickListener(this);
        get.setOnClickListener(this);
    }

    private void initData() {
        //路径是为了测试给图片添加exif信息所能接受的路径，[注：]记得加SD存储权限
        String sdPath = Environment.getExternalStorageDirectory().getPath() + "/DCIM/Camera/1.jpg";
        String sdCachePath = getExternalCacheDir().getPath() + "/1.jpg";//JHEAD: can't open '/storage/sdcard0/Android/data/com.zhang.exifdemo/cache/1.jpg'
        String dataCachePath = getCacheDir().getPath() + "/1.jpg";//JHEAD: can't open '/data/data/com.zhang.exifdemo/cache/1.jpg'

        //--------------测试结果表明，sdPath目录是可以对exif信息进行读写的，其他目录会报错如：/JHEAD: can't open/...------------------//

        //将SD卡中的照片写到sd卡缓存中
        imageIO(sdPath, sdCachePath);
        //将SD卡中的照片写到应用包名下的缓存中
        imageIO(sdPath, dataCachePath);

        path = sdPath;
        Log.e("zhang", " path-->" + path);

        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void imageIO(String target, String desc) {
        if (!TextUtils.isGraphic(target) && !TextUtils.isEmpty(desc)) {
            File targetFile = new File(target);
            File descFile = new File(desc);
            OutputStream os = null;
            InputStream is = null;
            try {
                is = new FileInputStream(targetFile);
                os = new FileOutputStream(descFile);
                int len = -1;
                byte[] bytes = new byte[1024];
                while ((len = is.read(bytes)) != -1) {
                    os.write(bytes, 0, len);
                }
                os.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (os != null) {
                        os.close();
                    }
                    if (is != null) {
                        is.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void initRecycler() {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        adapter = new MyAdapter(getApplicationContext(), mlists);
        recyclerView.setAdapter(adapter);
    }

    public void setExifInfo() {
        String content = edit.getText().toString().trim();
        if (encode) {//md5加密内容，这么做的原因是，大概知道每个tag所能保存的信息的长度
            content = MD5Utils.md5(content);
        }
        if (!TextUtils.isEmpty(content)) {
            setExifStringInfo(content);
            save();
            finish();
        }
    }

    public void save() {
        try {
            exif.saveAttributes();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_get:
                getExifInfo();
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                } else {
                    adapter = new MyAdapter(this, mlists);
                }
                break;
            case R.id.btn_set:
                encode = false;
                setExifInfo();
                break;

            case R.id.btn_set_md5:
                encode = true;
                setExifInfo();
                break;
        }
    }
}
