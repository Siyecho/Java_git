
import mediautil.gen.directio.SplitInputStream;

import mediautil.image.jpeg.LLJTran;

import mediautil.image.jpeg.LLJTranException;

import java.io.*;

public class CopyPictureHeader
{

    public static void main(String[] args)
       {
          //
       InputStream inOld = null;
          SplitInputStream sipOld = null;
      InputStream subIpOld = null;
       LLJTran lljOld = null;
       //
      InputStream inNew = null;
      SplitInputStream sipNew = null;
       InputStream subIpNew = null;
       LLJTran lljNew = null;

        OutputStream out = null;
		
      try
        {
			
                 //
                    inOld = new BufferedInputStream(new FileInputStream("d:/old.jpg"));
                    sipOld = new SplitInputStream(inOld);
                    subIpOld = sipOld.createSubStream();
             lljOld = new LLJTran(subIpOld);
             lljOld.initRead(LLJTran.READ_HEADER, true, true);
              sipOld.attachSubReader(lljOld, subIpOld);
               sipOld.wrapup();

              out = new BufferedOutputStream(new FileOutputStream("d:/new.jpg"));
             lljOld.xferInfo(inOld, out, LLJTran.REPLACE, LLJTran.REPLACE);

                //
                 inNew = new BufferedInputStream(new FileInputStream("d:/new.jpg"));
              sipNew = new SplitInputStream(inNew);
               subIpNew = sipOld.createSubStream();
            lljNew = new LLJTran(subIpNew);
              lljNew.initRead(LLJTran.READ_HEADER, true, true);
            sipNew.attachSubReader(lljNew, subIpNew);
              sipNew.wrapup();

              }
			  
           catch (FileNotFoundException e)
             {
              e.printStackTrace();
             }
           catch (LLJTranException e)
            {
               e.printStackTrace();
           }
           catch (IOException e)
                {
                 e.printStackTrace();
                }
              finally
			  
               //
               {
                   try
                 {
                       if (inOld != null)
                     {
                          inOld.close();
                       }
                        if (sipOld != null)
                     {
                         sipOld.close();
                 }
                        if (subIpOld != null)
                       {
                         subIpOld.close();
                      }
                       if (lljOld != null)
                       {
                          lljOld.freeMemory();
                     }
                       if (out != null)
                     {
                           out.close();
                       }
                 }
             catch (IOException e)
               {
                     e.printStackTrace();
                   }
             }
        }

		
        }
