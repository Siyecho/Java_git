import java.util.Scanner;

public class totalChar
{
	public static void main(String[] args)
	{
		Scanner scan=new Scanner(System.in);
		
		System.out.println("Enter a string here: ");
		String str=scan.nextLine();
		
		int len=str.length();
		int index=0,total=0;
		int a=0,e=0,i=0,o=0,u=0,other=0;
		
		for(;len>0;len--)
		{
			char ch=str.charAt(index++);
			total++;
			
			switch(ch)
			{
				case 'a':
					a++;
					break;
				case 'e':
					e++;
					break;
				case 'i':
					i++;
					break;
				case 'o':
					o++;
					break;
				case 'u':
					u++;
					break;
				default:
					other++;
			}
		}
		
		System.out.println("The character 'a' occurs "+a+" times.");
		System.out.println("The character 'e' occurs "+e+" times.");
		System.out.println("The character 'i' occurs "+i+" times.");
		System.out.println("The character 'o' occurs "+o+" times.");
		System.out.println("The character 'u' occurs "+u+" times.");
		System.out.println("Other characters occur "+other+" times.");
		System.out.println("All characters occur "+total+" times.");
	}
}