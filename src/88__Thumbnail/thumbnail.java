public class thumbnail4
{
    public static void main(String args[]) throws InvalidKeyException, IOException, NoSuchAlgorithmException
    {
        //char ch;
        //System.out.println();
        
        Scanner addr = new Scanner(System.in);
        System.out.println("enter the filename of photo");
        String filename = addr.next();
        
        //String filename = "hahaha.jpeg";
        //String filename = "hahaha.png";
        Scanner blocksize = new Scanner(System.in);
        System.out.println("enter the block size");
        int bsize = blocksize.nextInt();
        if( bsize <= 0)
        {
            System.out.println("block size could not be less or equal to zero");
            return;
        }
        Scanner subblocksize = new Scanner(System.in);
        System.out.println("enter the subblock size,if no subblock is need, enter zero");
        int subsize = subblocksize.nextInt();
        if( subsize < 0 | subsize > bsize)
        {
            System.out.println("subblock size could not be less to zero or larger to block size");
            return;
        }
        Scanner choice = new Scanner(System.in);
        System.out.println("enter the choice to encrypt or decrypt, 1 for encrypt, 2 for decrypt");
        int cho = choice.nextInt();
        
        Thumbnails thb = new Thumbnails(filename,bsize,subsize);
        if(cho == 1)
        {
            thb.encrypt();
        }
        else if(cho == 2)
        {
            thb.decrypt();
        }
        else
        {
            System.out.println("wrong input in choice to encrypt or decrypt");
            return;
        }
    }
}