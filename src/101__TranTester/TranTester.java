
package mediautil.test;

import java.io.*;
import java.util.Date;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
import javax.imageio.stream.ImageInputStream;

import mediautil.gen.directio.*;
import mediautil.image.ImageResources;
import mediautil.image.jpeg.LLJTran;
import mediautil.image.jpeg.Exif;
import mediautil.image.jpeg.Entry;
import mediautil.image.jpeg.LLJTranException;

public class LLJTranTester {

    public static void javaApi(InputStream ip, OutputStream op, OutputStream op1)
        throws IOException
    {
        ImageReader reader;
        ImageInputStream iis = ImageIO.createImageInputStream(ip);
        reader = (ImageReader) ImageIO.getImageReaders(iis).next();
        reader.setInput(iis);
        System.out.println("Reader = " + reader);
System.out.println("********** BEGIN READ: " + new Date());
        BufferedImage image = reader.read(0);
System.out.println("********** END READ: " + new Date());
        Graphics graphics = image.getGraphics();
        graphics.setColor(new Color(240, 100, 255, 255));
        graphics.drawString("Highlight " + new Date(), 20, image.getHeight() - 40);
        // Save modified image
        String format = "JPG";
        ImageIO.write(image, format, op);
        if(op1 != null)
            ImageIO.write(image, format, op1);
        iis.close();
    }

    public static LLJTran readShared(String fileName, boolean keep_appxs)
        throws LLJTranException, IOException
    {
        FileInputStream fip = new FileInputStream(fileName);
        SplitInputStream sip = new SplitInputStream(fip);
        InputStream subIp = sip.createSubStream();
        LLJTran llj = new LLJTran(subIp);
        llj.initRead(LLJTran.READ_ALL, keep_appxs, true);
        sip.attachSubReader(llj, subIp);

        // Read Image and load LLJTran
        BufferedOutputStream fop = new BufferedOutputStream(new FileOutputStream("d.jpg"));
        javaApi(sip, fop, null);
System.out.println("Max Blocks = " + sip.getMaxBufSize());
System.out.println(" minRead = " + llj.getRequestSize(0) + " maxRead = " + llj.getRequestSize(1));

        sip.wrapup();
        fip.close();
        fop.close();

        System.out.println("frm_x = " + llj.getWidth() + " frm_y = " + llj.getHeight()
                        + " maxHi = " + llj.getMaxHSamplingFactor() + " maxVi = " + llj.getMaxVSamplingFactor()
                        + " widthMCU = " + llj.getWidthInMCU() + " heightMCU = " + llj.getHeightInMCU());
        System.out.println("Info = " + llj.getImageInfo());
        System.out.println("Successfully Read Image");

        return llj;
    }

    public static void readImage(LLJTran llj, boolean keep_appxs, int stage1,
                            int stage2, int stage3)
        throws LLJTranException
    {
        if(stage1 != 0)
            llj.read(stage1, keep_appxs);
        if(stage2 != 0)
            llj.read(stage2, keep_appxs);
        System.out.println("frm_x = " + llj.getWidth() + " frm_y = " + llj.getHeight()
                        + " maxHi = " + llj.getMaxHSamplingFactor() + " maxVi = " + llj.getMaxVSamplingFactor()
                        + " widthMCU = " + llj.getWidthInMCU() + " heightMCU = " + llj.getHeightInMCU());
        if(stage3 != 0)
            llj.read(stage3, keep_appxs);
        System.out.println("Info = " + llj.getImageInfo());
        System.out.println("Successfully Read Image");
    }

    public static void main1(String[] args) throws Exception {
        LLJTran llj = new LLJTran(new File(args[0]));
        readImage(llj, true, LLJTran.READ_ALL, 0, 0);
        Rectangle cropArea = new Rectangle();
        byte newThumbnail[] = new byte[100000];
        int l;
        if(llj.getImageInfo().getThumbnailLength() > 0)
        {
            FileOutputStream top = new FileOutputStream("del.jpg");
            InputStream tip = llj.getThumbnailAsStream();
            while ((l=tip.read(newThumbnail)) > 0)
                top.write(newThumbnail, 0, l);
            top.close();
        }
        else
            System.out.println("Image has no Thumbnail");
        if(llj.getImageInfoAppxIndex() < 0)
        {
            System.out.println("Attempting to add Dummy Exif Header");
            llj.addAppx(LLJTran.dummyExifHeader, 0,
                        LLJTran.dummyExifHeader.length, true);
        }
        FileInputStream fip = new FileInputStream("x1.jpg");
        l = fip.read(newThumbnail); fip.close();
        llj.setThumbnail(newThumbnail, 0, l,
                             ImageResources.EXT_JPG);
        int i;
        String currentOpName, opName = args[1], prefix = null, suffix = null;
        if(args.length > 3)
        {
            i = opName.lastIndexOf('.');
            if(i < 0)
                i = opName.length();
            prefix = opName.substring(0, i) + '_';
            suffix = opName.substring(i);
        }

        currentOpName = opName;
        for(i=2; i < args.length; ++i)
        {
            int options = LLJTran.OPT_DEFAULTS | LLJTran.OPT_XFORM_ORIENTATION | LLJTran.OPT_XFORM_TRIM;
            int transformOp = Integer.parseInt(args[i]);
System.out.println("Transform begun at " + new Date());
            if(transformOp == LLJTran.CROP)
            {
                cropArea.x = Integer.parseInt(args[++i]);
                cropArea.y = Integer.parseInt(args[++i]);
                cropArea.width = Integer.parseInt(args[++i]);
                cropArea.height = Integer.parseInt(args[++i]);
                llj.transform(transformOp, options, cropArea);
            }
            else
                llj.transform(transformOp, options);
System.out.println("Transform ends at " + new Date());
            FileOutputStream op = new FileOutputStream(currentOpName);
            llj.save(op, LLJTran.OPT_WRITE_ALL);
            op.close();
            currentOpName = prefix + (i-1) + suffix;
        }
    }

    }
}
