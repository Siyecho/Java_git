//************************************************************
//
//	StyleOptions.java		Java Foundations
//
//	Demonstrates the use of check boxes.
//
//************************************************************

import javax.swing.JFrame;

public class StyleOptions
{
	//--------------------------------------------------------
	//	Creates and displays the style options frame.
	//--------------------------------------------------------
	public static void main(String[] args)
	{
		//----------------------------------------------------
		//	JFrame的构造方法带有一个字符串类型的参数，框架的
		//	标题栏将显示这个字符串
		//----------------------------------------------------
		JFrame frame = new JFrame ("Style Options");
		
		//----------------------------------------------------
		//	确定点击关闭按钮时执行什么操作
		//----------------------------------------------------
		frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		
		//----------------------------------------------------
		//	getContentPane得到框架的内容窗格
		//	add将面板添加到内容窗格中
		//----------------------------------------------------
		frame.getContentPane().add (new StyleOptionsPanel());
		
		//----------------------------------------------------
		//	pack()将根据框架中的内容把框架设置为合适的尺寸
		//----------------------------------------------------
		frame.pack();
		
		//----------------------------------------------------
		//	setVisible()用来在屏幕上显示框架
		//----------------------------------------------------
		frame.setVisible(true);
	}
}