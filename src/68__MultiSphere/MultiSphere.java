import java.util.Scanner;

public class MultiSphere {
    public static void main (String[] args)
    {
        Scanner scan = new Scanner(System.in);

        System.out.println("Input a diameter: ");
        double dia = scan.nextDouble();
        Sphere ball = new Sphere(dia);

        String str = ball.toString();
        System.out.println(str);
    }
}
